package com.avisi.bitbucketconfigurator.dao;

import com.avisi.bitbucketconfigurator.domain.objects.Log;

import java.util.List;

public interface LogDao {

    void create(final Log... items);

    List<Log> findAll();
}
