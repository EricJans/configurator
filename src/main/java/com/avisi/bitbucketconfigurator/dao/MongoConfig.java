package com.avisi.bitbucketconfigurator.dao;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.jongo.Jongo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Arrays;

@Configuration
@PropertySource("application.properties")
public class MongoConfig {

    @Value("${database_server_address}")
    private String serverAddress;

    @Value("${database_server_port}")
    private int serverPort;

    @Value("${database_name}")
    private String databaseName;

    @Value("${database_username}")
    private String username;

    @Value("${database_password}")
    private String password;

    @Bean
    public MongoClient mongoClient() {
        MongoCredential credential = MongoCredential.createCredential(username, databaseName, password.toCharArray());
        return new MongoClient(new ServerAddress(
                serverAddress, serverPort), Arrays.asList(credential));
    }

    @Bean
    public Jongo jongo() {
        return new Jongo(mongoClient().getDB(databaseName));
    }

}
