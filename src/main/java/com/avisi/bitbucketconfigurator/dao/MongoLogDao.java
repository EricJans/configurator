package com.avisi.bitbucketconfigurator.dao;

import com.avisi.bitbucketconfigurator.domain.objects.Log;
import com.google.common.collect.Lists;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("logDao")
public class MongoLogDao implements LogDao {

    private static final String COLLECTION_NAME = "logs";
    private final Jongo jongo;

    @Autowired
    public MongoLogDao(final Jongo jongo) {
        this.jongo = jongo;
    }

    private MongoCollection getCollection() {
        return jongo.getCollection(COLLECTION_NAME);
    }

    @Override
    public void create(final Log... items) {
        getCollection().insert(items);
    }

    @Override
    public List<Log> findAll() {
        return Lists.newArrayList(
                getCollection().find()
                        .as(Log.class)
                        .iterator());
    }
}
