package com.avisi.bitbucketconfigurator.domain.exception;

public class BitbucketCommunicationException extends Exception {

    public BitbucketCommunicationException(final String message) {
        super(message);
    }

}
