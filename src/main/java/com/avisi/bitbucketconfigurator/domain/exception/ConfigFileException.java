package com.avisi.bitbucketconfigurator.domain.exception;

import java.util.HashMap;
import java.util.Map;

public class ConfigFileException extends Exception {
    private final Map<String, Object> properties;

    public ConfigFileException(String message) {
        super(message);
        properties = new HashMap<>();
    }

    public Map<String, Object> getProperties() {
        return properties;
    }
}