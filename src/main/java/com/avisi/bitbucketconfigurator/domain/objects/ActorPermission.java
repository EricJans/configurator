package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ActorPermission implements Changeable{

    private final String name;

    private final PermissionType type;

    @JsonCreator
    protected ActorPermission(@JsonProperty("name") final String name,
                              @JsonProperty("type") final PermissionType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public PermissionType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        else if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        ActorPermission actorPermission = (ActorPermission) o;
        return Objects.equals(name, actorPermission.name) &&
                type.equals( actorPermission.type);
    }

    @Override
    public int hashCode() {
        return 10;
    }

    @Override
    public String toString() {
        return String.format("ActorPermission{ name=%s , type=%s }",name, type);
    }

    @Override
    public Map<String, Object> changeableValues() {
        Map<String, Object> map = new HashMap<>();

        map.put("permission_type", type);
        return map;
    }
}
