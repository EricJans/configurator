package com.avisi.bitbucketconfigurator.domain.objects;


import java.util.ArrayList;
import java.util.List;

public class ActorPermissionList extends ArrayList<ActorPermission> {

    /**
     * Algorithm to get a list of changes needed to be executed without revoking and adding all permissions. This only receives necessary changes.
     *
     * @param stale A list of the old Permissions that this list gets compared to.
     * @return A list of the changes between the this list and the given list.
     */
    public ActorPermissionList getChangesFrom(final ActorPermissionList stale) {
        ActorPermissionList changes = new ActorPermissionList();
        // This loop loops over all the stale items and check if they still exist in the fresh list.
        // If not, it adds them to the remove part of the list
        getChanges(stale).forEach(change -> {
            changes.add((ActorPermission) change.getNewObject());
        });
        return changes;
    }

    /**
     * Loops through all the stale actorpermissions and checks if the a stale permission is found in the new actor permission list     *
     * @param stale the stale list to get the actors
     * @return a list of all changes from the actor permissions so change is properly logged.
     */
    public List<Change> getChanges(final ActorPermissionList stale) {
        List<Change> changes = new ArrayList<>();

        for (ActorPermission old : stale) {

            if (!this.contains(old) && !containsName(old.getName())) {

                ActorPermission up = null;
                if (old instanceof UserPermission) {
                    up = new UserPermission(old.getName(), PermissionType.NONE);
                } else if (old instanceof GroupPermission) {
                    up = new GroupPermission(old.getName(), PermissionType.NONE);
                }
                if (up != null) {
                    changes.add(new Change(old, up));
                }
            }
        }
        //This loop loops over the fresh permissions and checks if the they exist in the stale list, if not it adds it to the list of changes
        for (ActorPermission permissions : this) {
            if (!stale.contains(permissions) && !changes.contains(permissions)) {
                changes.add(new Change(stale.get(permissions.getName()), permissions));
            }
        }

        return changes;

    }

    /**
     * Method that checks if a group exists in a Permissions list.
     *
     * @param item The name to be checked if it exists.
     * @return true if the given group exists in this list of permissions, or false if not.
     */
    public boolean containsName(String item) {
        for (ActorPermission permissions : this) {
            if (permissions.getName().equals(item)) {
                return true;
            }
        }
        return false;

    }

    /**
     * Checks if the name of an actor from the stale list is found in the new actor permission list
     * @param name user name of the actor
     * @return the actorpermission object so we can use it for the old value
     */
    public ActorPermission get(String name) {
        for (ActorPermission actorPermission : this) {
            if (actorPermission.getName().equals(name)) {
                return actorPermission;
            }
        }
        return null;
    }
}
