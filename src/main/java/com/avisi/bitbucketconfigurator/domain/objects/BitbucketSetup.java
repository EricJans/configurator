package com.avisi.bitbucketconfigurator.domain.objects;

import com.cdancy.bitbucket.rest.BitbucketClient;

public class BitbucketSetup {

    private final String project;
    private final String repository;
    private final BitbucketClient client;

    public BitbucketSetup(final BitbucketClient client, final String project, final String repository) {
        this.client = client;
        this.project = project;
        this.repository = repository;
    }

    public BitbucketClient getClient() {
        return client;
    }

    public String getProject() {
        return project;
    }

    public String getRepository() {
        return repository;
    }

    public String getUrlRepresentation() {
        return "/rest/api/1.0/projects/" + project + "/repos/" + repository;
    }
}