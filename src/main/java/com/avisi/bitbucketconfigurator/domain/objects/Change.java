package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Change {

    private final Changeable oldObject;
    private final Changeable newObject;

    private final ChangeType type;

    public Change(Changeable oldObject, Changeable newValueObject) {
        this.oldObject = oldObject;
        this.newObject = newValueObject;

        if (oldObject == null && newObject == null) {
            throw new IllegalArgumentException("Can't create a change with two null references!");
        }
        if (newObject.equals(oldObject)) {
            throw new IllegalArgumentException("These objects are the same!");

        }
        if (oldObject == null) {
            type = ChangeType.CREATE;
        } else if (newObject == null) {
            type = ChangeType.DELETE;
        } else {
            type = ChangeType.UPDATE;
        }


    }

    @JsonProperty("old_values")
    public Changeable getOldObject() {
        return oldObject;
    }

    @JsonProperty("new_values")
    public Changeable getNewObject() {
        return newObject;
    }

    @JsonProperty("change_type")
    public ChangeType getType() {
        return type;
    }
}
