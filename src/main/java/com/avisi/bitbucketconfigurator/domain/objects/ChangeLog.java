package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChangeLog {

    private final List<Change> actorPermissionList;
    private final List<Change> webhookSet;

    @JsonCreator
    public ChangeLog(@JsonProperty("added") final List<Change> actorPermissionList,
                     @JsonProperty("removed") final List<Change> webhookSet) {
        this.actorPermissionList = actorPermissionList;
        this.webhookSet = webhookSet;
    }

    public List<Change> getActorPermissionList() {
        return actorPermissionList;
    }

    public List<Change> getWebhookSet() {
        return webhookSet;
    }

}