package com.avisi.bitbucketconfigurator.domain.objects;

public enum ChangeType {
    UPDATE, DELETE, CREATE
}
