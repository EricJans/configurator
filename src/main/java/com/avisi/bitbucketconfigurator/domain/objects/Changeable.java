package com.avisi.bitbucketconfigurator.domain.objects;

import java.util.Map;

public interface Changeable {

    Map<String, Object> changeableValues();
}
