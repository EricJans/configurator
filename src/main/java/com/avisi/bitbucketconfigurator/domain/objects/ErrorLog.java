package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorLog {

    private final String message;
    private final String context;

    @JsonCreator
    public ErrorLog(@JsonProperty("message") final String message,
                    @JsonProperty("exception") final String context) {
        this.message = message;
        this.context = context;
    }

    public String getMessage() {
        return message;
    }

    public String getContext() {
        return context;
    }
}
