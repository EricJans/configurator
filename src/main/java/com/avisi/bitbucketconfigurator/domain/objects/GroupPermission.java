package com.avisi.bitbucketconfigurator.domain.objects;

import com.cdancy.bitbucket.rest.domain.repository.Group;
import com.cdancy.bitbucket.rest.domain.repository.Permissions;

public class GroupPermission extends ActorPermission {

    public GroupPermission(final Group group, final PermissionType type) {
        super(group.name(), type);
    }

    public GroupPermission(final String group, final PermissionType type) {
        super(group, type);
    }

    public GroupPermission(final Permissions permissions) {
        super(permissions.group().name(), PermissionType.getEnum(permissions.permission().name()));
    }

}