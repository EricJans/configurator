package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import java.util.Date;


public class Log {

    private final Date date;
    private final String project;
    private final String repository;
    private final String username;
    private final ChangeLog changeLog;
    private final ErrorLog errorLog;

    @JsonCreator
    public Log(@JsonProperty("date") final Date date,
               @JsonProperty("project") final String project,
               @JsonProperty("repository") final String repository,
               @JsonProperty("username") final String username,
               @JsonProperty("changeLog") @Nullable final ChangeLog changeLog,
               @JsonProperty("errorLog") @Nullable final ErrorLog errorLog) {
        this.date = date;
        this.project = project;
        this.repository = repository;
        this.username = username;
        this.changeLog = changeLog;
        this.errorLog = errorLog;
    }

    public Date getDate() {
        return date;
    }

    public String getProject() {
        return project;
    }

    public String getRepository() {
        return repository;
    }

    public String getUsername() {
        return username;
    }

    @Nullable
    public ChangeLog getChangeLog() {
        return changeLog;
    }

    @Nullable
    public ErrorLog getErrorLog() {
        return errorLog;
    }
}
