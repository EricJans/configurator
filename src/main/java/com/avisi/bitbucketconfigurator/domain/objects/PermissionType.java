package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.logging.log4j.util.Strings;

@JsonDeserialize(using = PermissionTypeDeserializer.class)
public enum PermissionType {
    READ("REPO_READ", 1), WRITE("REPO_WRITE", 2), ADMIN("REPO_ADMIN", 3), NONE(Strings.EMPTY, 0);


    private final String command;
    private final int rank;

    PermissionType(final String command, int rank) {
        this.command = command;
        this.rank = rank;
    }

    public static PermissionType getEnum(final String enumName) {
        return valueOf(enumName.trim().toUpperCase().replaceAll("REPO_",""));
    }
    public String getCommand() {
        return command;
    }

    public int getRank() {
        return rank;
    }
}