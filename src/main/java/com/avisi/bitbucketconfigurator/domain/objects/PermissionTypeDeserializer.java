package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

public class PermissionTypeDeserializer extends JsonDeserializer<PermissionType> {
    @Override
    public PermissionType deserialize(JsonParser jp, DeserializationContext ctx) throws IOException, JsonProcessingException {
        return PermissionType.getEnum(jp.getValueAsString());
    }
}
