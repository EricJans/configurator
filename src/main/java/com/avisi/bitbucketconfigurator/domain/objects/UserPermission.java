package com.avisi.bitbucketconfigurator.domain.objects;

import com.cdancy.bitbucket.rest.domain.pullrequest.User;
import com.cdancy.bitbucket.rest.domain.repository.Permissions;

public class UserPermission extends ActorPermission {

    public UserPermission(final User user, final PermissionType type) {
        super(user.name(), type);
    }

    public UserPermission(final String user, final PermissionType type) {
        super(user, type);
    }

    public UserPermission(final Permissions permissions) {
        super(permissions.user().name(), PermissionType.getEnum(permissions.permission().name()));
    }

}