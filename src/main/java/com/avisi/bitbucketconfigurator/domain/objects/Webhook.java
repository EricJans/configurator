package com.avisi.bitbucketconfigurator.domain.objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.emory.mathcs.backport.java.util.Collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Webhook implements Changeable {

    private final String id;
    private final String name;
    private String url;
    private final List<String> events;

    @JsonCreator
    public Webhook(
            @JsonProperty(value = "name") String name,
            @JsonProperty(value = "url", required = true) String url,
            @JsonProperty(value = "events", required = true) List<String> events) {
        this.name = name;
        this.url = url;
        Collections.sort(events);
        this.events = events;

        StringBuilder stringBuilder = new StringBuilder(url);
        stringBuilder.append(url);

        events.forEach(stringBuilder::append);
        this.id = stringBuilder.toString();

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public List<String> getEvents() {
        return events;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Webhook webhook = (Webhook) o;

        String url = this.url.trim();
        if (!url.equals(webhook.url)) {
            return false;
        }
        if (events != null) {
            return events.containsAll(webhook.events);
        } else {
            return webhook.events == null;
        }
    }

    @Override
    public String toString() {
        return String.format("Webhook { id=%s, name=%s, url=%s, events=%s }", id, name, url, events);
    }



    @Override
    public int hashCode() {
        return 10;
    }

    @Override
    public Map<String, Object> changeableValues() {
        Map<String, Object> map = new HashMap<>();

        map.put("name", getName());
        return map;
    }

}
