package com.avisi.bitbucketconfigurator.domain.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class WebhookSet extends HashSet<Webhook> {

    public WebhookSet getChangesFrom(final WebhookSet stale) {
        WebhookSet changes = new WebhookSet();
        getChanges(stale).forEach(change -> {
            changes.add((Webhook) change.getNewObject());
        });
        return changes;
    }

    /**
     * Builds a list of changed webhooks with a stale (old) set of webhooks.
     * @param stale the webhookset to loop through
     * @return a list of webhook that do not exist in de stale webhooks
     */
    public List<Change> getChanges(final WebhookSet stale) {
        List<Change> changes = new ArrayList<>();
        for (Webhook change : this) {
            if (!stale.contains(change)) {
                changes.add(new Change(webhookMatchInStaleList(stale, change), change));
            }
        }
        return changes;
    }

    /**
     * If no match is found the index value returns -1. This means there is a new webhook.
     * When a name matches we retrieve the previous values and set them as old.
     *
     * @param staleList  a list that contains the latest webhooks from the server
     * @param newWebhook a webhook
     * @return false when there is no match based on de staleIndex or true when a match is found and breaks from the loop.
     */
    private Webhook webhookMatchInStaleList(final Collection<Webhook> staleList, final Webhook newWebhook) {

        for (Webhook staleWebhook : staleList) {
            if (isWebhookEqual(newWebhook, staleWebhook)) {
                return staleWebhook;
            }
        }

        return null;
    }

    /**
     * Webhook with the same name found: there was a modification.
     *
     * @param newWebhook
     * @param staleWebhook
     * @return true if a new webhook matches with a webhook in the stale list by name.
     */
    private boolean isWebhookEqual(Webhook newWebhook, Webhook staleWebhook) {
        return newWebhook.
                getName().
                equals(staleWebhook.getName());
    }

}
