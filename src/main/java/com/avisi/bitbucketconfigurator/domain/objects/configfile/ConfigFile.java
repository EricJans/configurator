package com.avisi.bitbucketconfigurator.domain.objects.configfile;

import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermissionList;
import com.avisi.bitbucketconfigurator.domain.objects.GroupPermission;
import com.avisi.bitbucketconfigurator.domain.objects.UserPermission;
import com.avisi.bitbucketconfigurator.domain.objects.Webhook;
import com.avisi.bitbucketconfigurator.domain.objects.WebhookSet;
import com.cdancy.bitbucket.rest.domain.pullrequest.User;
import com.cdancy.bitbucket.rest.domain.repository.Group;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigFile {

    private PermissionWrapper permissions = new PermissionWrapper();

    private Map<String, Webhook> webhooks = new HashMap<>();


    public PermissionWrapper getPermissions() {
        return permissions;
    }

    public void setPermissions(PermissionWrapper permissions) {
        this.permissions = permissions;
    }

    public void setWebhooks(Map<String, Webhook> webhooks) {
        this.webhooks = webhooks;
    }

    public Map<String, Webhook> getWebhooks() {
        return webhooks;
    }

    /**
     * Creates a list of Webhooks from the ConfigFile.
     *
     * @return A list of Webhooks extracted from the ConfigFile.
     */
    public WebhookSet createWebhookList() throws ConfigFileException {
        WebhookSet webhookSet = new WebhookSet();
        final List<String> nonValidWebhook = new ArrayList<>();

        webhooks.forEach((key, value) -> {

            if (isValidURL(value.getUrl())) {
                Webhook webhook = new Webhook(key, value.getUrl(), value.getEvents());
                webhookSet.add(webhook);
            } else {
                nonValidWebhook.add(key);
            }

        });

        if (nonValidWebhook.size() > 0) {
            throw new ConfigFileException(String.format("Invalid Webhook(s) with name: %s", nonValidWebhook));
        }

        return webhookSet;
    }

    /**
     * Creates a list of PermissionChanges from the ConfigFile.
     *
     * @return A list containing the permissions specified in the config file.
     */
    public ActorPermissionList createPermissionList() {
        ActorPermissionList list = new ActorPermissionList();

        permissions.getUsers().forEach((key, value) -> {
            User user = User.create(key, "", 0, "", true, "", "");
            UserPermission up = new UserPermission(user, value);
            list.add(up);
        });
        permissions.getGroups().forEach((key, value) -> {
            Group group = Group.create(key);
            GroupPermission up = new GroupPermission(group, value);
            list.add(up);
        });
        return list;
    }

    private boolean isValidURL(String url) {
        URL u;

        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            return false;
        }

        try {
            u.toURI();
        } catch (URISyntaxException e) {
            return false;
        }

        return true;
    }

}