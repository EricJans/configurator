package com.avisi.bitbucketconfigurator.domain.objects.configfile;

import com.avisi.bitbucketconfigurator.domain.objects.PermissionType;

import java.util.Map;

public class PermissionWrapper {

    private Map<String, PermissionType> users;

    private Map<String, PermissionType> groups;

    public Map<String, PermissionType> getUsers() {
        return users;
    }

    public void setUsers(final Map<String, PermissionType> users) {
        this.users = users;
    }

    public Map<String, PermissionType> getGroups() {
        return groups;
    }

    public void setGroups(final Map<String, PermissionType> groups) {
        this.groups = groups;
    }

}
