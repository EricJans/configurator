package com.avisi.bitbucketconfigurator.domain.services;

import com.cdancy.bitbucket.rest.BitbucketClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BitbucketClientService {

    private String url;
    private String username;
    private String password;

    public BitbucketClientService(@Value("${bitbucket_url}") String url,
                                  @Value("${bitbucket_username}") String username,
                                  @Value("${bitbucket_password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * Reads from the bitbucket.properties file in order to create a client to the Bitbucket API using cdancy's "bitbucket-rest" library.
     * @return BitbucketClient from cdancy's "bitbucket-rest" library
     */
    public BitbucketClient getBitbucketClient(){

        final BitbucketClient bitbucketClient = BitbucketClient.builder()
                .endPoint(url)
                .credentials(username + ":" + password)
                .build();

        return bitbucketClient;
    }

}