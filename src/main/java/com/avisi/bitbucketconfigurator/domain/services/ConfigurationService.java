package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermission;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermissionList;
import com.avisi.bitbucketconfigurator.domain.objects.BitbucketSetup;
import com.avisi.bitbucketconfigurator.domain.objects.Change;
import com.avisi.bitbucketconfigurator.domain.objects.ChangeLog;
import com.avisi.bitbucketconfigurator.domain.objects.GroupPermission;
import com.avisi.bitbucketconfigurator.domain.objects.PermissionType;
import com.avisi.bitbucketconfigurator.domain.objects.UserPermission;
import com.avisi.bitbucketconfigurator.domain.objects.Webhook;
import com.avisi.bitbucketconfigurator.domain.objects.WebhookSet;
import com.avisi.bitbucketconfigurator.domain.objects.configfile.ConfigFile;
import com.avisi.bitbucketconfigurator.domain.objects.configfile.PermissionWrapper;
import com.avisi.bitbucketconfigurator.gateway.BitbucketGateway;
import com.cdancy.bitbucket.rest.features.RepositoryApi;
import org.springframework.beans.factory.annotation.Autowired;
import com.cdancy.bitbucket.rest.domain.repository.PermissionsPage;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class ConfigurationService {

    private final YamlService yamlService;

    private final BitbucketGateway bitbucketGateway;

    @Autowired
    public ConfigurationService(final YamlService yamlService, final BitbucketGateway bitbucketGateway) {
        this.yamlService = yamlService;
        this.bitbucketGateway = bitbucketGateway;
    }

    private ConfigFile mergeConfigFile(ConfigFile configFile, ConfigFile defaultConfigFile) {
        final ConfigFile mergedConfigFile = new ConfigFile();
        final PermissionWrapper permissionWrapper = new PermissionWrapper();

        permissionWrapper.setUsers(mergeUserPermissions(configFile, defaultConfigFile));
        permissionWrapper.setGroups(mergeGroupPermissions(configFile, defaultConfigFile));

        mergedConfigFile.setPermissions(permissionWrapper);
        mergedConfigFile.setWebhooks(new HashMap<>());
        mergeWebhooks(configFile, defaultConfigFile).forEach(webhook->{
            mergedConfigFile.getWebhooks().put(webhook.getName(), webhook);

        });
        return mergedConfigFile;
    }

    private Map<String, PermissionType> mergeUserPermissions(ConfigFile configFile, ConfigFile defaultConfigFile) {
        final Map<String, PermissionType> configUserPermissions = configFile.getPermissions().getUsers();
        final Map<String, PermissionType> defaultUserPermissions = defaultConfigFile.getPermissions().getUsers();

        return comparePermissions(configUserPermissions, defaultUserPermissions);
    }

    private Map<String, PermissionType> mergeGroupPermissions(ConfigFile configFile, ConfigFile defaultConfigFile) {
        final Map<String, PermissionType> configGroupPermissions = configFile.getPermissions().getGroups();
        final Map<String, PermissionType> defaultGroupPermissions = defaultConfigFile.getPermissions().getGroups();

        return comparePermissions(configGroupPermissions, defaultGroupPermissions);
    }

    private Set<Webhook> mergeWebhooks(ConfigFile configFile, ConfigFile defaultConfigFile) {
        final Map<String, Webhook> configWebhooks = configFile.getWebhooks();
        final Map<String, Webhook> defaultWebhooks = defaultConfigFile.getWebhooks();

        return compareWebhooks(configWebhooks, defaultWebhooks);
    }

    private Set<Webhook> compareWebhooks(Map<String, Webhook> configWebhooks, Map<String, Webhook> defaultWebhooks) {
        final Set<Webhook> mergedWebhooks = new HashSet<>();
        if (defaultWebhooks != null) {
            defaultWebhooks.forEach((key, value) -> mergedWebhooks.add(new Webhook(key, value.getUrl(), value.getEvents())));
        }
        if (configWebhooks != null) {
            configWebhooks.forEach((key, value) -> mergedWebhooks.add(new Webhook(key, value.getUrl(), value.getEvents())));
        }

        return mergedWebhooks;
    }

    private Map<String, PermissionType> comparePermissions(Map<String, PermissionType> configPermissions,
                                                           Map<String, PermissionType> defaultPermissions) {

        final Map<String, PermissionType> mergedPermissions = new HashMap<>();

        if (configPermissions != null) {
            mergePermissions(configPermissions, defaultPermissions, mergedPermissions);
        }

        if (defaultPermissions != null) {
            defaultPermissions.forEach((key, value) -> {
                if (configPermissions == null) {
                    mergedPermissions.put(key, value);
                } else if (!configPermissions.containsKey(key)) {
                    mergedPermissions.put(key, value);
                }
            });
        }
        return mergedPermissions;
    }

    private void mergePermissions(Map<String, PermissionType> configPermissions,
                                  Map<String, PermissionType> defaultPermissions,
                                  Map<String, PermissionType> mergedPermissions) {

        configPermissions.forEach((key, value) -> {
            if (defaultPermissions == null) {
                mergedPermissions.put(key, value);
            } else if (defaultPermissions.containsKey(key) &&
                    defaultPermissions.get(key).getRank() > configPermissions.get(key).getRank()) {
                mergedPermissions.put(key, defaultPermissions.get(key));
            } else {
                defaultPermissions.put(key, defaultPermissions.get(key));
                mergedPermissions.put(key, value);
            }
        });
    }



    /**
     * Updates all the configurations specified in the given yaml configuration string.
     * @param bitbucketSetup The BitbucketSetup to which this configuration has to be applied to.
     * @param yamlConfiguration The specified configuration string formatted as YAML.
     * @throws ConfigFileException When mapping the configuration onto a config file fails.
     * @throws BitbucketCommunicationException When communication with Bitbucket fails.
     * @return A Log containing all the changes that occurred.
     */
    public ChangeLog updateAllConfigurations(final BitbucketSetup bitbucketSetup, final String yamlConfiguration, final InputStream filePath) throws BitbucketCommunicationException, ConfigFileException {

        final ConfigFile configFile = yamlService.deserializeYaml(yamlConfiguration);
        final ConfigFile defaultConfigFile = yamlService.deserializeYamlFile(filePath);

        final ConfigFile mergedConfigFile = mergeConfigFile(configFile, defaultConfigFile);

        final List<Change> actorPermissionChanges = retrieveUpdatePermissions(bitbucketSetup, mergedConfigFile);
        final List<Change> webhookChanges = retrieveUpdateWebhooks(bitbucketSetup, mergedConfigFile);


        for (Change change : actorPermissionChanges) {
            executeChange(bitbucketSetup, (ActorPermission) change.getNewObject());
        }
        bitbucketGateway.createWebhooksFromChangeList(webhookChanges, bitbucketSetup);

        return new ChangeLog(actorPermissionChanges, webhookChanges);
    }

    /**
     * Updates Permissions based on a ConfigFile into the Bitbucket server.
     * @param bitbucketSetup The bitbucket setup used for interacting with the Bitbucket server.
     * @param configFile The ConfigFile that holds the new configuration.
     * @return A list of the applied changes.
     */
    protected List<Change> retrieveUpdatePermissions(final BitbucketSetup bitbucketSetup, final ConfigFile configFile) {

        final ActorPermissionList yamlConfigPermissions = configFile.createPermissionList();
        final ActorPermissionList onlinePermissions = getOnlinePermissions(bitbucketSetup);

        final List<Change> changes = yamlConfigPermissions.getChanges(onlinePermissions);

        return changes;
    }

    /**
     * Updates all webhooks based on a config file
     * @param bitbucketSetup The bitbucket setup used for interacting with the Bitbucket server.
     * @param configFile The ConfigFile that holds the new configuration.
     * @return A list of changes
     * @throws BitbucketCommunicationException
     */
    protected List<Change> retrieveUpdateWebhooks(final BitbucketSetup bitbucketSetup, final ConfigFile configFile) throws BitbucketCommunicationException, ConfigFileException {

        final WebhookSet yamlConfigWebhooks =  configFile.createWebhookList();
        final WebhookSet onlineWebhooks = bitbucketGateway.retrieveWebhooks(bitbucketSetup);

        final List<Change> changes = yamlConfigWebhooks.getChanges(onlineWebhooks);

        return changes;
    }

    /**
     * Executes the changes that have been made to the YAML file
     * @param change A list of all the changes that have been made to the YAML file
     */
    protected void executeChange(final BitbucketSetup bitbucketSetup, final ActorPermission change) {

        final RepositoryApi repositoryApi = bitbucketSetup.getClient().api().repositoryApi();

        if (change instanceof UserPermission) {
            if (change.getType().equals(PermissionType.NONE)) {
                repositoryApi.deletePermissionsByUser(bitbucketSetup.getProject(), bitbucketSetup.getRepository(), change.getName());
            } else {
                repositoryApi.createPermissionsByUser(bitbucketSetup.getProject(), bitbucketSetup.getRepository(), change.getType().getCommand(), change.getName());
            }
        } else if (change instanceof GroupPermission) {
            if (change.getType().equals(PermissionType.NONE)) {
                repositoryApi.deletePermissionsByGroup(bitbucketSetup.getProject(), bitbucketSetup.getRepository(), change.getName());
            } else {
                repositoryApi.createPermissionsByGroup(bitbucketSetup.getProject(), bitbucketSetup.getRepository(), change.getType().getCommand(), change.getName());
            }
        }
    }

    /**
     * Gets all the permissions that are online at this moment
     * @return A list of the online permissions
     */
    protected ActorPermissionList getOnlinePermissions(final BitbucketSetup bitbucketSetup) {
        final RepositoryApi repositoryApi = bitbucketSetup.getClient().api().repositoryApi();

        final ActorPermissionList online = new ActorPermissionList();
        final PermissionsPage permissionsPageUsers = repositoryApi.listPermissionsByUser(bitbucketSetup.getProject(), bitbucketSetup.getRepository(), null, null);
        final PermissionsPage permissionsPageGroups = repositoryApi.listPermissionsByGroup(bitbucketSetup.getProject(), bitbucketSetup.getRepository(), null, null);

        permissionsPageUsers
                .values()
                .forEach(permissions -> online.add(new UserPermission(permissions)));

        permissionsPageGroups
                .values()
                .forEach(permissions -> online.add(new GroupPermission(permissions)));

        return online;
    }
}