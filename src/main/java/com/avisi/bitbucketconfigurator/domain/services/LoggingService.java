package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.dao.LogDao;
import com.avisi.bitbucketconfigurator.domain.objects.Log;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoggingService {

    private final LogDao logDao;

    @Autowired
    public LoggingService(final LogDao logDao) {
        this.logDao = logDao;
    }

    public List<Log> getLogs() {
        return Lists.newArrayList(logDao.findAll());
    }

    public void addToLogs(Log log) {
        logDao.create(log);
    }
}