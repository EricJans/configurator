package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.BitbucketSetup;
import com.avisi.bitbucketconfigurator.domain.objects.ChangeLog;
import com.avisi.bitbucketconfigurator.domain.objects.ErrorLog;
import com.avisi.bitbucketconfigurator.domain.objects.Log;
import com.avisi.bitbucketconfigurator.rest.dto.PushPayloadDTO;
import com.cdancy.bitbucket.rest.BitbucketClient;
import com.cdancy.bitbucket.rest.domain.common.Error;
import com.cdancy.bitbucket.rest.domain.file.RawContent;
import com.cdancy.bitbucket.rest.features.FileApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;

@Service
public class PayloadService {

    private static final Logger LOG = LoggerFactory.getLogger(PayloadService.class);

    private final BitbucketClientService bitbucketClientService;
    private final LoggingService loggingService;
    private final ConfigurationService configurationService;

    private String configurationFileName;

    @Autowired
    public PayloadService(final BitbucketClientService bitbucketClientService,
                          final LoggingService loggingService,
                          final ConfigurationService configurationService,
                          @Value("${yaml_configuration_file_url}") final String configurationFileName) {
        this.bitbucketClientService = bitbucketClientService;
        this.loggingService = loggingService;
        this.configurationService = configurationService;
        this.configurationFileName = configurationFileName;
    }

    /**
     * Call this method to delegate logic for a push event.
     * @param pushPayload The DTO containing the push payload send by the webhook.
     */
    public void handle(final PushPayloadDTO pushPayload) {

        ChangeLog changeLog = null;
        ErrorLog errorLog = null;

        try {
            final BitbucketSetup bitbucketSetup = createBitbucketSetup(pushPayload);

            final String yamlConfigurationString = retrieveConfigFile(bitbucketSetup);

            changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlConfigurationString, this.getClass().getResourceAsStream("/defaultconfig.yaml"));

        } catch (ConfigFileException e) {
            final String errorMessage = String.format("Invalid YAML or configuration in %s.", configurationFileName);
            LOG.error(errorMessage, e);
            errorLog = new ErrorLog(errorMessage, e.getMessage());
        } catch (BitbucketCommunicationException e) {
            final String errorMessage = "Failure in communication with the Bitbucket server";
            LOG.error(errorMessage, e);
            errorLog = new ErrorLog(errorMessage, e.getMessage());
        }

        Log log = new Log(pushPayload.getDate(),
                pushPayload.getRepository().getProject().getKey(),
                pushPayload.getRepository().getName(),
                pushPayload.getActor().getName(),
                changeLog, errorLog);

        loggingService.addToLogs(log);
    }

    /**
     * Create a BitbucketSetup by using the BitbucketClientService and extracting the data from the PushPayloadDTO.
     * @param pushPayload the payload that has been pushed.
     * @return BitbucketSetup with the correct parameters.
     */
    private BitbucketSetup createBitbucketSetup(final PushPayloadDTO pushPayload) {
        final BitbucketClient bitbucketClient = bitbucketClientService.getBitbucketClient();

        // Get information of which project and repo this was pushed to.
        final String projectKey = pushPayload.getRepository().getProject().getKey();
        final String repoKey = pushPayload.getRepository().getName();

        return new BitbucketSetup(bitbucketClient, projectKey, repoKey);
    }

    /**
     * Retrieves the raw YAML content or throws a file not found exception when it fails to retrieve the file.
     * @param bitbucketSetup the BitbucketSetup that has been created.
     * @return String containing the raw content of the config.yaml file.
     * @throws FileNotFoundException When failed to read the file contents. Due to, for example, failing to locate the file.
     */
    public String retrieveConfigFile(final BitbucketSetup bitbucketSetup) throws BitbucketCommunicationException {

        final FileApi fileApi = bitbucketSetup.getClient().api().fileApi();

        // Commit hash is null, because this takes the latest commit.
        final RawContent rawContent = fileApi.raw(
                bitbucketSetup.getProject(),
                bitbucketSetup.getRepository(),
                configurationFileName,
                null);

        for (Error error : rawContent.errors()) {
            LOG.error(error.message(), error.context());
        }

        if (rawContent.errors().size() > 0) {
            throw new BitbucketCommunicationException("Failed to retrieve config file content.");
        }

        return rawContent.value();
    }

}