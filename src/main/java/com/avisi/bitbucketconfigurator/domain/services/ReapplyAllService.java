package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermissionList;
import com.avisi.bitbucketconfigurator.domain.objects.BitbucketSetup;
import com.avisi.bitbucketconfigurator.domain.objects.ChangeLog;
import com.avisi.bitbucketconfigurator.domain.objects.ErrorLog;
import com.avisi.bitbucketconfigurator.domain.objects.Log;
import com.cdancy.bitbucket.rest.domain.project.Project;
import com.cdancy.bitbucket.rest.domain.project.ProjectPage;
import com.cdancy.bitbucket.rest.domain.repository.Repository;
import com.cdancy.bitbucket.rest.domain.repository.RepositoryPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReapplyAllService {

    private static final Logger LOG = LoggerFactory.getLogger(ReapplyAllService.class);

    private static final String BITBUCKET_CONFIGURATOR_REAPPLY_ALL_USERNAME = "BITBUCKET_CONFIGURATOR_REAPPLY_ALL";

    private final BitbucketClientService client;
    private final PayloadService payloadService;
    private final ConfigurationService configurationService;

    @Autowired
    public ReapplyAllService(final BitbucketClientService client,
                             final ConfigurationService configurationService,
                             final PayloadService payloadService) {
        this.client = client;
        this.configurationService = configurationService;
        this.payloadService = payloadService;
    }

    /**
     * Gets all repositories from all projects.
     * Updates all configurations for all repositories with a Config.yaml file
     *
     * @return A list of logs as a result of the configuration updates.
     */
    public List<Log> updateAllConfigurations() {
        final List<Log> logs = new ArrayList<Log>();

        final ProjectPage projects = client.getBitbucketClient().api().projectApi().list(null, null, null, null);
        for (Project project : projects.values()) {
            RepositoryPage repositories = client.getBitbucketClient().api().repositoryApi().list(project.key(), null, null);
            updateRepositories(project, repositories.values(), logs);
        }

        return logs;
    }

    /**
     * Updates all configurations in all repositories with a Config.yaml file
     * @param project      The project where the repositorie is stored on
     * @param repositories A list of all repositories on a project
     * @param logs The list where changelogs should be placed into.
     * @return A Changelog with all the changes that have to be made
     */
    private void updateRepositories(Project project, List<Repository> repositories, List<Log> logs) {
        ChangeLog changeLog;
        ErrorLog errorLog;
        Log log;

        BitbucketSetup bitbucketSetup;

        for (Repository repository : repositories) {
            changeLog = null;
            errorLog = null;
            bitbucketSetup = new BitbucketSetup(client.getBitbucketClient(), project.key(), repository.slug());

            try {
                final String configFile = payloadService.retrieveConfigFile(bitbucketSetup);
                changeLog = configurationService.updateAllConfigurations(bitbucketSetup, configFile, this.getClass().getResourceAsStream("/defaultconfig.yaml"));

            } catch (BitbucketCommunicationException e) {
                final String errorMessage = String.format("No configuration file found in repository %s", repository.name());
                errorLog = new ErrorLog(errorMessage, e.getMessage());
                LOG.info(errorMessage);

            } catch (ConfigFileException e) {
                final String errorMessage = String.format("Invalid YAML configuration file in repository %s", repository.name());
                e.getProperties().put("repo", repository.name());
                errorLog = new ErrorLog(errorMessage, e.getMessage());
                LOG.info(errorMessage);

            } finally {
                log = new Log(
                        new Date(),
                        project.key(),
                        repository.slug(),
                        BITBUCKET_CONFIGURATOR_REAPPLY_ALL_USERNAME,
                        changeLog, errorLog);
                logs.add(log);
            }
        }
    }

}