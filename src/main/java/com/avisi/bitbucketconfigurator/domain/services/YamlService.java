package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.PermissionType;
import com.avisi.bitbucketconfigurator.domain.objects.Group;
import com.avisi.bitbucketconfigurator.domain.objects.configfile.ConfigFile;
import com.avisi.bitbucketconfigurator.gateway.BitbucketGateway;
import com.cdancy.bitbucket.rest.domain.pullrequest.User;
import com.cdancy.bitbucket.rest.features.AdminApi;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class YamlService {

    private static final Logger LOG = LoggerFactory.getLogger(YamlService.class);
    private BitbucketGateway bitbucketGateway;

    public YamlService(){}

    @Autowired
    public YamlService(BitbucketGateway bitbucketGateway) {
        this.bitbucketGateway = bitbucketGateway;
    }

    /**
     * Deserialize a raw YAML string from the configuration file into a ConfigFile instance.
     *
     * @param yamlConfiguration Tha raw YAML string
     * @return A ConfigFile containing a representation of the YAML configuration string
     * @throws ConfigFileException When the YAML fails to deserialize into a ConfigFile.
     */
    public ConfigFile deserializeYaml(final String yamlConfiguration) throws ConfigFileException {
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            return mapper.readValue(yamlConfiguration, ConfigFile.class);
        } catch (IOException e) {
            throw new ConfigFileException(String.format("Failed to map YAML configuration into a ConfigFile: %s", e.getMessage()));
        }
    }

    public ConfigFile deserializeYamlFile(final String filePath) throws ConfigFileException {
        return deserializeYamlFile(this.getClass().getResourceAsStream(filePath));
    }
    public ConfigFile deserializeYamlFile(final InputStream file) throws ConfigFileException {
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            return mapper.readValue(file, ConfigFile.class);
        } catch (IOException e) {
            throw new ConfigFileException(String.format("Failed to map YAML configuration into a ConfigFile: %s", e.getMessage()));
        }
    }

    /**
     * Method to get the users from the bitbucket server
     *
     * @param bitbucketClientService Bitbucket client connection, to get users from bitbucket
     * @return List with users from the bitbucket server
     */
    private List<User> getBitbucketUsers(final BitbucketClientService bitbucketClientService) {
        final AdminApi api = bitbucketClientService.getBitbucketClient().api().adminApi();
        final List<User> userList = api.listUsers(null, null, null).values();
        return userList;
    }

    /**
     * Method to convert User objects to a string value, for comparisson with yaml users.
     *
     * @param bitbucketUserList List of User objects from the Cdancy bitbucket-rest library
     * @return List of converted User strings.
     */
    private List<String> convertBitbucketUsers(List<User> bitbucketUserList) {
        final List<String> usernameList = new ArrayList<>();

        bitbucketUserList.forEach(u -> usernameList.add(u.name()));

        return usernameList;
    }

    /**
     * Method in which the YAML given to the URL is tested, to check if its valid YAML and if it has the required Users/Groups
     *
     * @param yamlString             YAML text in string format
     * @param bitbucketClientService Cdancy Bitbucket client, to get users from bitbucket
     * @throws ConfigFileException   Throws when there is an error with the mapping or when there are not found users/groups
     */
    public void validateYaml(final String yamlString, final BitbucketClientService bitbucketClientService) throws ConfigFileException, BitbucketCommunicationException {
        ConfigFile configFile = deserializeYaml(yamlString);
        final List<String> notFoundUsers = checkUsers(bitbucketClientService, configFile);
        final List<String> notFoundGroups = checkGroups(bitbucketClientService, configFile);

        if(notFoundUsers.size() > 0 && notFoundGroups.size() == 0) {
            throw new ConfigFileException(String.format("User(s) not found: %s", notFoundUsers.toString()));
        }
        else if (notFoundGroups.size() > 0 && notFoundUsers.size() == 0) {
            throw new ConfigFileException(String.format("Group(s) not found: %s", notFoundGroups.toString()));
        }
        else if(notFoundGroups.size() > 0 && notFoundUsers.size() > 0) {
            throw new ConfigFileException(String.format("User(s) not found: %s AND Group(s) not found: %s",notFoundUsers.toString(), notFoundGroups.toString()));
        }
    }

    /**
     * Checks if the users in the yaml file match the ones found on the bitbucket server
     *
     * @param bitbucketClientService Cdancy bitbucket client, to get users from bitbucker
     * @param configfile             Mapped YAML data to a configfile class.
     * @throws ConfigFileException   Throws when users in the YAML are not available on the server
     */
    private List<String> checkUsers(final BitbucketClientService bitbucketClientService, final ConfigFile configfile) throws ConfigFileException {
        final List<String> yamlUsers = getYamlUsers(configfile);
        final List<String> bitbucketUsers = convertBitbucketUsers(getBitbucketUsers(bitbucketClientService));
        final List<String> notFoundUsers = new ArrayList<>();

        for (String s : yamlUsers) {
            if (!bitbucketUsers.contains(s)) {
                notFoundUsers.add(s);
            }
        }

        return notFoundUsers;
    }

    private List<String> checkGroups(final BitbucketClientService bitbucketClientService, final ConfigFile configFile) throws BitbucketCommunicationException, ConfigFileException {
        final List<String> yamlGroups = getYamlGroups(configFile);
        final List<String> bitbucketGroups = convertBitbucketGroups(getBitbucketGroups(bitbucketClientService));
        final List<String> notFoundGroups = new ArrayList<>();

        for (String s : yamlGroups){
            if(!bitbucketGroups.contains(s)){
                notFoundGroups.add(s);
            }
        }

        return notFoundGroups;
    }

    private List<Group> getBitbucketGroups(final BitbucketClientService bitbucketClientService) throws BitbucketCommunicationException {

        final List<Group> bitbucketGroupList = bitbucketGateway.retrieveGroups(bitbucketClientService);

        return bitbucketGroupList;
    }

    /**
     * Method to get the users from the YAML file
     *
     * @param file The YAML File
     * @return A list with users from the YAML file.
     */
    private List<String> getYamlUsers(final ConfigFile file) {
        final List<String> yamlUserNames = new ArrayList<>();
        Map<String, PermissionType> map = file.getPermissions().getUsers();
        map.forEach((key, value) -> yamlUserNames.add(key));
        return yamlUserNames;
    }

    private List<String> convertBitbucketGroups(final List<Group> bitbucketGroupList){
        final List<String> groupNameList = new ArrayList<>();

        bitbucketGroupList.forEach(g -> groupNameList.add(g.getName()));

        return groupNameList;
    }

    private List<String> getYamlGroups(final ConfigFile file) {
        final List<String> yamlGroupNames = new ArrayList<>();
        final Map<String, PermissionType> map = file.getPermissions().getGroups();
        map.forEach((key, value) -> yamlGroupNames.add(key));
        return yamlGroupNames;
    }
}