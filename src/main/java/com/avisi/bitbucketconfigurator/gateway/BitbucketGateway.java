package com.avisi.bitbucketconfigurator.gateway;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.objects.BitbucketSetup;
import com.avisi.bitbucketconfigurator.domain.objects.Change;
import com.avisi.bitbucketconfigurator.domain.objects.Group;
import com.avisi.bitbucketconfigurator.domain.objects.Webhook;
import com.avisi.bitbucketconfigurator.domain.objects.WebhookSet;
import com.avisi.bitbucketconfigurator.domain.services.BitbucketClientService;
import com.avisi.bitbucketconfigurator.gateway.dto.BitbucketPageDTO;
import com.avisi.bitbucketconfigurator.gateway.dto.GroupDTO;
import com.avisi.bitbucketconfigurator.gateway.dto.PostWebhookDTO;
import com.avisi.bitbucketconfigurator.gateway.dto.WebhookDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class BitbucketGateway {

    private final ObjectMapper jsonObjectMapper = new ObjectMapper();

    /**
     * Retrieves a list of webhooks on the repository.
     *
     * @param bitbucketSetup For calling the Bitbucket API and querying the right project and repository.
     * @return A WebhookSet containing all webhooks found.
     * @throws BitbucketCommunicationException when communication fails through either cdancy or our gateway
     */
    public WebhookSet retrieveWebhooks(final BitbucketSetup bitbucketSetup) throws BitbucketCommunicationException {
        final WebhookSet webhookSet = new WebhookSet();

        final String getWebhookJson = sendGetWebhooksRequest(bitbucketSetup);
        final BitbucketPageDTO<WebhookDTO> getWebhookDTO = jsonToBitbucketPageDTO(getWebhookJson);

        for (WebhookDTO webhookDTO : getWebhookDTO.getValues()) {
            Webhook webhook = new Webhook(
                    webhookDTO.getName(),
                    webhookDTO.getUrl(),
                    webhookDTO.getEvents());
            webhookSet.add(webhook);
        }

        return webhookSet;
    }

    /**
     * Retrieves the group(s) that exists on the Bitbucket server
     * @param bitbucketClientService the bitbucket that is used for retrieving the project repository
     * @return A list of groups from our domain
     * @throws BitbucketCommunicationException when communication fails through either Cdancy or our gateway
     */
    public List<Group> retrieveGroups(final BitbucketClientService bitbucketClientService) throws BitbucketCommunicationException {
        List<Group> groups = new ArrayList<>();

        final String getGroupJson = sendGetGroupsRequest(bitbucketClientService);
        final  BitbucketPageDTO<GroupDTO> getGroupDTO = jsonGroupToBitbucketPageDTO(getGroupJson);

        for(GroupDTO groupDTO : getGroupDTO.getValues()) {
            Group group = new Group(groupDTO.getName(), groupDTO.isDeletable());
            groups.add(group);
        }

        return groups;
    }

    /**
     * Creates webhooks based on the new webhooks.
     * @param newWebhooks potential new webhooks added in yaml config file
     * @param bitbucketSetup the bitbucket  that is used for retrieving the project repository
     * @throws BitbucketCommunicationException when communication fails through either Cdancy or our gateway
     */
    public void createWebhooksFromChangeList(final List<Change> newWebhooks, final BitbucketSetup bitbucketSetup) throws BitbucketCommunicationException {

        WebhookSet webhookSet = new WebhookSet();
        newWebhooks.forEach(webhook -> {
            if (webhook.getNewObject() instanceof Webhook) {
                webhookSet.add((Webhook) webhook.getNewObject());
            }
        });
        createWebhooks(webhookSet, bitbucketSetup);
    }

    public void createWebhooks(final WebhookSet newWebhooks, final BitbucketSetup bitbucketSetup) throws BitbucketCommunicationException {
        for (Webhook webhook : newWebhooks) {
            PostWebhookDTO postWebhookDTO = new PostWebhookDTO(
                    webhook.getEvents(),
                    new HashMap<>(),
                    webhook.getName(),
                    webhook.getUrl(),
                    true);

            sendPostWebhookRequest(postWebhookDTO, bitbucketSetup);
        }
    }

    private String[] checkCredentials(BitbucketSetup bitbucketSetup) throws BitbucketCommunicationException{
        final String[] credentials = bitbucketSetup.getClient().credentials().split(":");
        if(credentials.length != 2){
            throw new BitbucketCommunicationException("Invalid Bitbucket credentials");
        } else{
            return credentials;
        }
    }

    private String sendGetGroupsRequest(final BitbucketClientService bitbucketClientService) throws BitbucketCommunicationException{
        final String[] credentials = bitbucketClientService.getBitbucketClient().credentials().split(":");
        if(credentials.length != 2){
            throw new BitbucketCommunicationException("Invalid Bitbucket credentials");
        }

        try{
            final HttpResponse<String> response = Unirest.get(formatGroupsUrl(bitbucketClientService))
                    .basicAuth(credentials[0], credentials[1])
                    .asString();

            return response.getBody();
        } catch(UnirestException e){
            throw new BitbucketCommunicationException(String.format("Failed to get groups from Bitbucket server: %s", e.getMessage()));
        }
    }

    private String sendGetWebhooksRequest(final BitbucketSetup bitbucketSetup) throws BitbucketCommunicationException {
        final String[] credentials = checkCredentials(bitbucketSetup);

        try {
            final HttpResponse<String> response = Unirest.get(formatWebhooksUrl(bitbucketSetup))
                    .basicAuth(credentials[0], credentials[1])
                    .asString();

            return response.getBody();

        } catch (UnirestException e) {
            throw new BitbucketCommunicationException(String.format("Failed to get webhooks from Bitbucket server: %s", e.getMessage()));
        }
    }

    private void sendPostWebhookRequest(final PostWebhookDTO postWebhookDTO, final BitbucketSetup bitbucketSetup) throws BitbucketCommunicationException {

        final String[] credentials = bitbucketSetup.getClient().credentials().split(":");
        if (credentials.length != 2) {
            throw new BitbucketCommunicationException("Invalid Bitbucket credentials");
        }

        HttpResponse<JsonNode> postResponse = null;
        try {
            postResponse = Unirest.post(formatWebhooksUrl(bitbucketSetup))
                    .basicAuth(credentials[0], credentials[1])
                    .header("Content-Type", "application/json;charset=UTF-8")
                    .body(jsonObjectMapper.writeValueAsString(postWebhookDTO))
                    .asJson();
        } catch (UnirestException | JsonProcessingException e) {
            throw new BitbucketCommunicationException(String.format("Failed in sending the post request for creating a webhook on the Bitbucket server: %s", e));
        }

        final int status = postResponse.getStatus();
        if (!(status >= 200 && status < 300)) {
            final String errorMessage = String.format("Failed to create webhook on post request. Status code %s returned on creating the webhook %s", postResponse.getStatus(), postWebhookDTO.getUrl());
            throw new BitbucketCommunicationException(errorMessage);
        }
    }

    private BitbucketPageDTO<WebhookDTO> jsonToBitbucketPageDTO(final String getWebhookJson) throws BitbucketCommunicationException {
        try {
            return jsonObjectMapper.readValue(getWebhookJson, new TypeReference<BitbucketPageDTO<WebhookDTO>>(){});
        } catch (IOException e) {
            throw new BitbucketCommunicationException(String.format("Failed to map JSON onto BitbucketPageDTO<WebhookDTO>: %s", e.getMessage()));
        }
    }

    private BitbucketPageDTO<GroupDTO> jsonGroupToBitbucketPageDTO(final String getGroupJson) throws BitbucketCommunicationException {
        try {
            return jsonObjectMapper.readValue(getGroupJson, new TypeReference<BitbucketPageDTO<GroupDTO>>(){});
        } catch (IOException e) {
            throw new BitbucketCommunicationException(String.format("Failed to map JSON onto BitbucketPageDTO<GroupDTO>: %s", e.getMessage()));
        }
    }

    private String formatWebhooksUrl(final BitbucketSetup bitbucketSetup) {
        return String.format("%s/rest/api/1.0/projects/%s/repos/%s/webhooks",
                bitbucketSetup.getClient().endPoint(),
                bitbucketSetup.getProject(),
                bitbucketSetup.getRepository());
    }

    private String formatGroupsUrl(final BitbucketClientService bitbucketClientService){
        return String.format("%s/rest/api/1.0/admin/groups",
                bitbucketClientService.getBitbucketClient().endPoint());
    }
}
