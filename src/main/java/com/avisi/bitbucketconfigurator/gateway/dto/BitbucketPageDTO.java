package com.avisi.bitbucketconfigurator.gateway.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BitbucketPageDTO<T> {
    private final int size;
    private final int limit;
    private final boolean isLastPage;
    private final List<T> values;
    private final int start;

    @JsonCreator
    public BitbucketPageDTO(@JsonProperty(value = "size", required = true) final int size,
                            @JsonProperty(value = "limit", required = true) final int limit,
                            @JsonProperty(value = "isLastPage", required = true) final boolean isLastPage,
                            @JsonProperty(value = "values", required = true) final List<T> values,
                            @JsonProperty(value = "start", required = true) final int start) {
        this.size = size;
        this.limit = limit;
        this.isLastPage = isLastPage;
        this.values = values;
        this.start = start;
    }

    public int getSize() {
        return size;
    }

    public int getLimit() {
        return limit;
    }

    @JsonProperty("isLastPage")
    public boolean isLastPage() {
        return isLastPage;
    }

    public List<T> getValues() {
        return values;
    }

    public int getStart() {
        return start;
    }
}
