package com.avisi.bitbucketconfigurator.gateway.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupDTO {
    private final String name;
    private final boolean deletable;

    @JsonCreator
    public GroupDTO(@JsonProperty(value = "name", required = true) final String name,
                 @JsonProperty(value = "deletable", required = true) final boolean deletable ) {
        this.name = name;
        this.deletable = deletable;
    }
    public String getName(){
        return name;
    }

    public boolean isDeletable() {
        return deletable;
    }
}