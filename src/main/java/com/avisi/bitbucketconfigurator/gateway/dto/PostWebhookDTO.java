package com.avisi.bitbucketconfigurator.gateway.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostWebhookDTO {

    private final List<String> events;
    private final Object configuration;
    private final String name;
    private final String url;
    private final boolean active;

    @JsonCreator
    public PostWebhookDTO(@JsonProperty(value = "events", required = true) List<String> events,
                          @JsonProperty(value = "configuration", required = true) Object configuration,
                          @JsonProperty(value = "name", required = true) String name,
                          @JsonProperty(value = "url", required = true) String url,
                          @JsonProperty(value = "active", required = true) boolean active) {
        this.events = events;
        this.configuration = configuration;
        this.name = name;
        this.url = url;
        this.active = active;
    }

    public List<String> getEvents() {
        return events;
    }

    public Object getConfiguration() {
        return configuration;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public boolean isActive() {
        return active;
    }
}
