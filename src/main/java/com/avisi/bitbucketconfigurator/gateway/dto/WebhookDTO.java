package com.avisi.bitbucketconfigurator.gateway.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebhookDTO {

    private final int id;
    private final String name;
    private final Date createdDate;
    private final Date updatedDate;
    private final List<String> events;
    private final Map<String, Object> configuration;
    private final String url;

    @JsonCreator
    public WebhookDTO(@JsonProperty(value = "id", required = true) final int id,
                      @JsonProperty(value = "name", required = true) final String name,
                      @JsonProperty(value = "createdDate", required = true) final  Date createdDate,
                      @JsonProperty(value = "updatedDate", required = true) final  Date updatedDate,
                      @JsonProperty(value = "events", required = true) final List<String> events,
                      @JsonProperty(value = "configuration", required = true) final Map<String, Object> configuration,
                      @JsonProperty(value = "url", required = true) final String url) {
        this.id = id;
        this.name = name;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.events = events;
        this.configuration = configuration;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public List<String> getEvents() {
        return events;
    }

    public Map<String, Object> getConfiguration() {
        return configuration;
    }

    public String getUrl() {
        return url;
    }

}
