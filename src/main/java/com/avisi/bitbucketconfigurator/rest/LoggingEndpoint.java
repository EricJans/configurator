package com.avisi.bitbucketconfigurator.rest;

import com.avisi.bitbucketconfigurator.domain.services.LoggingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoggingEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(LoggingEndpoint.class);

    private final LoggingService loggingService;

    private final ObjectMapper jsonObjectMapper = new ObjectMapper();

    @Autowired
    public LoggingEndpoint(final LoggingService loggingService) {
        this.loggingService = loggingService;
    }

    @RequestMapping("/logs")
    public String showAuditLogs() throws JsonProcessingException {
        try {
            return jsonObjectMapper.writeValueAsString(loggingService.getLogs());
        } catch (JsonProcessingException e) {
            LOG.error("Failed to write logs to JSON.", e);
            throw e;
        }
    }

}