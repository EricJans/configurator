package com.avisi.bitbucketconfigurator.rest;

import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.services.PayloadService;
import com.avisi.bitbucketconfigurator.domain.services.ReapplyAllService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReapplyAllEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(PayloadService.class);
    private final ReapplyAllService reapplyAllService;

    @Autowired
    public ReapplyAllEndpoint(final ReapplyAllService reapplyAllService) {
        this.reapplyAllService = reapplyAllService;
    }

    @RequestMapping(path = "/reapply-all")
    public void reapplyAll() {
        reapplyAllService.updateAllConfigurations();
    }

}
