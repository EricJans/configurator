package com.avisi.bitbucketconfigurator.rest;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.BitbucketSetup;
import com.avisi.bitbucketconfigurator.domain.services.BitbucketClientService;
import com.avisi.bitbucketconfigurator.domain.services.PayloadService;
import com.avisi.bitbucketconfigurator.domain.services.YamlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RepoConfigValidEndpoint {


    private final PayloadService payloadService;

    private final YamlService yamlService;

    private final BitbucketClientService bitbucketClientService;

    @Autowired
    public RepoConfigValidEndpoint(final PayloadService payloadService, final YamlService yamlService, final BitbucketClientService bitbucketClientService) {
        this.payloadService = payloadService;
        this.yamlService = yamlService;
        this.bitbucketClientService = bitbucketClientService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/validate/{project_key}/{repository_key}")
    public ResponseEntity<String> validateEndpoint(@PathVariable("project_key") final String projectKey, @PathVariable("repository_key") final String repoKey) {
        try {
            final String fileContents = payloadService.retrieveConfigFile(new BitbucketSetup(bitbucketClientService.getBitbucketClient(), projectKey, repoKey));
            yamlService.validateYaml(fileContents, bitbucketClientService);
            return ResponseEntity.ok("The config file on this endpoint is valid.");
        } catch (BitbucketCommunicationException | ConfigFileException e) {

            final String errorMessage = String.format("Invalid configuration YAML: %s", e.getMessage());
            return ResponseEntity.status(401).body(errorMessage);

        }
    }
}
