package com.avisi.bitbucketconfigurator.rest;

import com.avisi.bitbucketconfigurator.domain.services.PayloadService;
import com.avisi.bitbucketconfigurator.rest.dto.PushPayloadDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(path = "/webhook")
public class WebhookEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(WebhookEndpoint.class);

    private final ObjectMapper jsonObjectMapper = new ObjectMapper();

    private final PayloadService payloadService;

    @Autowired
    public WebhookEndpoint(final PayloadService payloadService) {
        this.payloadService = payloadService;
    }

    @RequestMapping(path = "/push", method = RequestMethod.POST)
    public void onWebhookPush(@RequestBody String pushPayloadString) {
        try {
            // We have to map the object in this method rather than directly as argument, because we want to log potential errors.
            PushPayloadDTO pushPayloadDTO = jsonObjectMapper.readValue(pushPayloadString, PushPayloadDTO.class);
            payloadService.handle(pushPayloadDTO);
        } catch (IOException e) {
            LOG.error("Failed to map PushPayload JSON onto Data Transfer Object." , e);
        }
    }
}