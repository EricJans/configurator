package com.avisi.bitbucketconfigurator.rest;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.services.BitbucketClientService;
import com.avisi.bitbucketconfigurator.domain.services.YamlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class YamlValidatorEndpoint {

    private final YamlService yamlService;

    private final BitbucketClientService bitbucketClientService;

    @Autowired
    public YamlValidatorEndpoint(final YamlService yamlService, final BitbucketClientService bitbucketClientService) {
        this.yamlService = yamlService;
        this.bitbucketClientService = bitbucketClientService;
    }

    /**
     * Tries to map a YAML string to an Java Object to see if it has a valid object structure, also checks if the given YAML is valid YAML.
     * @param yamlString The YAML string that is given.
     */
    @RequestMapping(path = "/validate-yaml", method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> validateYaml(@RequestBody String yamlString) {
        final Map<String, Object> responseMap = new HashMap<>();
        try {
            yamlService.validateYaml(yamlString, bitbucketClientService);
            responseMap.put("success_message", "The YAML config file is valid");
            return ResponseEntity.ok(responseMap);
        } catch (ConfigFileException e) {
            final String message = String.format("Invalid configuration YAML: %s", e.getMessage());
            responseMap.put("error_message", message);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseMap);
        } catch (BitbucketCommunicationException e) {
            responseMap.put("error_message", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseMap);
        }
    }
}