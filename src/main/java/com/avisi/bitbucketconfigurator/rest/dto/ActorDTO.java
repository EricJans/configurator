package com.avisi.bitbucketconfigurator.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ActorDTO {

    private final String name;
    private final String emailAddress;
    private final int id;
    private final String displayName;
    private final boolean active;
    private final String slug;
    private final String type;

    @JsonCreator
    public ActorDTO(@JsonProperty(value = "id", required = true) final int id,
                    @JsonProperty(value = "name", required = true) final String name,
                    @JsonProperty(value = "emailAddress", required = true) final String emailAddress,
                    @JsonProperty(value = "displayName", required = true) final String displayName,
                    @JsonProperty(value = "active", required = true) final boolean active,
                    @JsonProperty(value = "slug", required = true) final String slug,
                    @JsonProperty(value = "type", required = true) final String type) {
        this.id = id;
        this.name = name;
        this.emailAddress = emailAddress;
        this.displayName = displayName;
        this.active = active;
        this.slug = slug;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getSlug() {
        return slug;
    }

    public String getType() {
        return type;
    }

    public boolean isActive(){
        return active;
    }

}