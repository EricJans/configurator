package com.avisi.bitbucketconfigurator.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangeDTO {

    private final RefDTO ref;
    private final String refId;
    private final String fromHash;
    private final String toHash;
    private final String type;

    @JsonCreator
    public ChangeDTO(@JsonProperty(value = "ref", required = true) final RefDTO ref,
                     @JsonProperty(value = "refId", required = true) final String refId,
                     @JsonProperty(value = "fromHash", required = true) final String fromHash,
                     @JsonProperty(value = "toHash", required = true) final String toHash,
                     @JsonProperty(value = "type", required = true) final String type) {
        this.ref = ref;
        this.refId = refId;
        this.fromHash = fromHash;
        this.toHash = toHash;
        this.type = type;
    }

    public RefDTO getRef() {
        return ref;
    }

    public String getRefId() {
        return refId;
    }

    public String getFromHash() {
        return fromHash;
    }

    public String getToHash() {
        return toHash;
    }

    public String getType() {
        return type;
    }

}