package com.avisi.bitbucketconfigurator.rest.dto;

import java.util.Map;

public class ConfigFileDTO {

    Map<String,String> permission;
    Map<String,String> webhook;

    public ConfigFileDTO (
            final Map<String, String> permission,
            final Map<String, String> webhook) {
        this.permission = permission;
        this.webhook = webhook;
    }

    public Map<String, String> getPermission() {
        return permission;
    }

    public Map<String, String> getWebhook() {
        return webhook;
    }

}
