package com.avisi.bitbucketconfigurator.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectDTO {

    private final String key;
    private final int id;
    private final String name;
    private final boolean isPublic;
    private final String type;
    private final String description;

    @JsonCreator
    public ProjectDTO(@JsonProperty(value = "key", required = true) final String key,
                      @JsonProperty(value = "id", required = true) final int id,
                      @JsonProperty(value = "name", required = true) final String name,
                      @JsonProperty(value = "public", required = true) final boolean isPublic,
                      @JsonProperty(value = "type", required = true) final String type,
                      @JsonProperty(value = "description") final String description) {
        this.key = key;
        this.id = id;
        this.name = name;
        this.isPublic = isPublic;
        this.type = type;
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }
}