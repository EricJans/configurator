package com.avisi.bitbucketconfigurator.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class PushPayloadDTO {

    private final String eventKey;
    private final Date date;
    private final ActorDTO actor;
    private final RepositoryDTO repository;
    private final List<ChangeDTO> changes;

    @JsonCreator
    public PushPayloadDTO(@JsonProperty(value = "eventKey", required = true) final String eventKey,
                          @JsonProperty(value = "date", required = true) final Date date,
                          @JsonProperty(value = "actor", required = true) final ActorDTO actor,
                          @JsonProperty(value = "repository", required = true) final RepositoryDTO repository,
                          @JsonProperty(value = "changes", required = true) final List<ChangeDTO> changes) {
        this.eventKey = eventKey;
        this.date = date;
        this.actor = actor;
        this.repository = repository;
        this.changes = changes;
    }

    public String getEventKey() {
        return eventKey;
    }

    public Date getDate() {
        return date;
    }

    public ActorDTO getActor() {
        return actor;
    }

    public RepositoryDTO getRepository() {
        return repository;
    }

    public List<ChangeDTO> getChanges() {
        return changes;
    }

}