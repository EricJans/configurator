package com.avisi.bitbucketconfigurator.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RefDTO {

    private final String id;
    private final String displayId;
    private final String type;

    @JsonCreator
    public RefDTO(@JsonProperty(value = "id", required = true) final String id,
                  @JsonProperty(value = "displayId", required = true) final String displayId,
                  @JsonProperty(value = "type", required = true) final String type) {
        this.id = id;
        this.displayId = displayId;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getDisplayId() {
        return displayId;
    }

    public String getType() {
        return type;
    }

}