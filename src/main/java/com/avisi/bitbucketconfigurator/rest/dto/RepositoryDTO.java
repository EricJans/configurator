package com.avisi.bitbucketconfigurator.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RepositoryDTO {

    private final String slug;
    private final int id;
    private final String name;
    private final String scmId;
    private final String state;
    private final String statusMessage;
    private final boolean forkable;
    private final ProjectDTO project;
    private final boolean isPublic;

    @JsonCreator
    public RepositoryDTO(@JsonProperty(value = "slug", required = true) final String slug,
                         @JsonProperty(value = "id", required = true) final int id,
                         @JsonProperty(value = "name", required = true) final String name,
                         @JsonProperty(value = "scmId", required = true) final String scmId,
                         @JsonProperty(value = "state", required = true) final String state,
                         @JsonProperty(value = "statusMessage", required = true) final String statusMessage,
                         @JsonProperty(value = "forkable", required = true) final boolean forkable,
                         @JsonProperty(value = "project", required = true) final ProjectDTO project,
                         @JsonProperty(value = "public", required = true) final boolean isPublic) {
        this.slug = slug;
        this.id = id;
        this.name = name;
        this.scmId = scmId;
        this.state = state;
        this.statusMessage = statusMessage;
        this.forkable = forkable;
        this.project = project;
        this.isPublic = isPublic;
    }

    public String getSlug() {
        return slug;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getScmId() {
        return scmId;
    }

    public String getState() {
        return state;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public boolean isForkable() {
        return forkable;
    }

    public ProjectDTO getProject() {
        return project;
    }

    public boolean isPublic() {
        return isPublic;
    }

}