package com.avisi.bitbucketconfigurator;

import com.avisi.bitbucketconfigurator.domain.objects.ActorPermissionList;
import com.avisi.bitbucketconfigurator.domain.objects.PermissionType;
import com.avisi.bitbucketconfigurator.domain.objects.UserPermission;
import com.cdancy.bitbucket.rest.domain.pullrequest.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ActorPermissionListTest {

    private ActorPermissionList staleList;
    private User ryan;
    private User jesse;
    private User marnix;
    private UserPermission jessePermission;
    private UserPermission marnixPermission;

    @Before
    public void setUp() {
        ryan = User.create("Ryan", "", 0, "", true, "", "");

        jesse = User.create("Jesse", "", 1, "", true, "", "");
        jessePermission = new UserPermission(jesse, PermissionType.ADMIN);

        marnix = User.create("Marnix", "", 2, "", true, "", "");
        marnixPermission = new UserPermission(marnix, PermissionType.WRITE);

        staleList = new ActorPermissionList();
        staleList.add(marnixPermission);
        staleList.add(new UserPermission(ryan, PermissionType.NONE));
    }

    @Test
    public void testPermissionChanges() {
        ActorPermissionList freshList = new ActorPermissionList();

        freshList.addAll(staleList);

        freshList.add(jessePermission);
        freshList.remove(marnixPermission);

        ActorPermissionList changes = freshList.getChangesFrom(staleList);

        assertEquals(changes.size() ,2);
        assertTrue(changes.contains(jessePermission) );
        assertTrue( changes.containsName(marnix.name()));

    }

    @Test
    public void testListContainsUser() {
        assertTrue(staleList.containsName(ryan.name()));
    }

    @Test
    public void testListDoesntContainUser() {
        assertFalse(staleList.containsName(jesse.name()));
    }

}
