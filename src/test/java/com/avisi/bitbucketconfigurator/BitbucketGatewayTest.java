package com.avisi.bitbucketconfigurator;

import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.objects.BitbucketSetup;
import com.avisi.bitbucketconfigurator.domain.objects.Webhook;
import com.avisi.bitbucketconfigurator.domain.objects.WebhookSet;
import com.avisi.bitbucketconfigurator.gateway.BitbucketGateway;
import com.avisi.bitbucketconfigurator.gateway.dto.BitbucketPageDTO;
import com.avisi.bitbucketconfigurator.gateway.dto.WebhookDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

public class BitbucketGatewayTest extends BitbucketTest {
    private BitbucketSetup setup;

    @Before
    public void setUp() throws Exception {

        WebhookDTO sampleHook = new ObjectMapper().readValue("{\n" +
                "            \"id\": 10,\n" +
                "            \"name\": \"Webhook Name\",\n" +
                "            \"createdDate\": 1513106011000,\n" +
                "            \"updatedDate\": 1513106011000,\n" +
                "            \"events\": [\n" +
                "                \"repo:refs_updated\",\n" +
                "                \"repo:modified\"\n" +
                "            ],\n" +
                "            \"configuration\": {\n" +
                "                \"secret\": \"password\"\n" +
                "            },\n" +
                "            \"url\": \"http://example.com\",\n" +
                "            \"active\": true\n" +
                "        }", WebhookDTO.class);
        BitbucketPageDTO<WebhookDTO> page = new BitbucketPageDTO<>(3, 3, false, new ArrayList<WebhookDTO>() {{
            add(sampleHook);
        }}, 0);
        setup = new BitbucketSetup(client, "PROJECT_1", "rep_1");

        stubFor(post(setup.getUrlRepresentation() + "/webhooks")
                .willReturn(status(201)));
        returnPageFromUrl(setup.getUrlRepresentation() + "/webhooks", page);
    }

    @Test
    public void testRetrieveWebhooks() throws BitbucketCommunicationException {
        BitbucketGateway gw = new BitbucketGateway();

        WebhookSet webhooks = gw.retrieveWebhooks(setup);
        assertEquals(1, webhooks.size());
        assertEquals("Webhook Name", webhooks.toArray(new Webhook[webhooks.size()])[0].getName());
    }

    @Test
    public void testCreateWebhooks() throws BitbucketCommunicationException, IOException {

        BitbucketGateway gw = new BitbucketGateway();

        gw.createWebhooks(new WebhookSet() {{
            add(new Webhook("newHook", "/kfek", new ArrayList<String>() {{
                add("repo:refs_updated");
            }}));
        }}, setup);

        List<LoggedRequest> requests = findAll(postRequestedFor(urlMatching(setup.getUrlRepresentation() + "/webhooks")));

        assertEquals(1, requests.size());
        Webhook webhook = new ObjectMapper().readValue(requests.get(0).getBodyAsString(), Webhook.class);
        assertEquals("newHook", webhook.getName());
    }

}
