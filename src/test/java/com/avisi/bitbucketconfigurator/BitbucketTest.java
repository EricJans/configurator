package com.avisi.bitbucketconfigurator;

import com.avisi.bitbucketconfigurator.domain.services.BitbucketClientService;
import com.avisi.bitbucketconfigurator.gateway.dto.BitbucketPageDTO;
import com.cdancy.bitbucket.rest.BitbucketClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public abstract class BitbucketTest {

    protected BitbucketClient client;
    protected BitbucketClientService clientService;


    @ClassRule
    public static WireMockRule wm = new WireMockRule(options().port(2345).bindAddress("localhost").extensions(new ResponseTransformer() {
        @Override
        public String getName() {
            return "Bitbucket Mock Server Response";
        }

        @Override
        public Response transform(Request request, Response response, FileSource fileSource, Parameters parameters) {
            return new Response.Builder()
                    .body(response.getBodyAsString())
                    .build();
        }
    }));

    @Before
    public void init() throws Exception {

        client = BitbucketClient.builder()
                .endPoint("http://localhost:2345/")
                .credentials("username:password")
                .build();

        clientService = new BitbucketClientService("http://localhost:2345/", "", "");

    }

    protected void returnPageFromUrl(String url, BitbucketPageDTO page) throws JsonProcessingException {
        stubFor(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(new ObjectMapper().writeValueAsString(page))));
    }

    protected void returnPageFromUrl(String url, String fileName) throws JsonProcessingException {
        stubFor(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile(fileName)));
    }

}