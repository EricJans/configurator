package com.avisi.bitbucketconfigurator;

import com.avisi.bitbucketconfigurator.dao.LogDao;
import com.avisi.bitbucketconfigurator.dao.MongoLogDao;
import com.avisi.bitbucketconfigurator.domain.objects.Log;
import com.github.fakemongo.Fongo;
import org.jongo.Jongo;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.*;

public class LogDaoTest {

    private LogDao logDao;
    private Jongo jongo;

    @Before
    public void setUp() throws Exception {
        Fongo fongo = new Fongo("log-test");
        jongo = new Jongo(fongo.getDB("log-test"));
        logDao = new MongoLogDao(jongo);
    }

    @Test
    public void testCreateLog() {
        Log log = new Log(null, "test", null, null, null, null);

        logDao.create(log);

        assertEquals(1, jongo.getCollection("logs").count());
        assertNotNull(jongo.getCollection("logs").findOne("{project: 'test'}"));
    }

    @Test
    public void testFindAllLogs() {

        Log log = new Log(null, "test", null, null, null, null);

        logDao.create(log);

        List<Log> all = logDao.findAll();
        assertEquals(1, all.size());
    }
}
