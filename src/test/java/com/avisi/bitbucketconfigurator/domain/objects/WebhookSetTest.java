package com.avisi.bitbucketconfigurator.domain.objects;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WebhookSetTest {

    private WebhookSet staleList;
    private Webhook testWebhook1;
    private Webhook testWebhook2;
    private List<String> eventsWebhook1;
    private List<String> eventsWebhook2;

    @Before
    public void setup(){
        eventsWebhook1 = new ArrayList<>();
        eventsWebhook1.add("refs_changed");
        eventsWebhook2 = new ArrayList<>();
        eventsWebhook2.add("");

        testWebhook1 = new Webhook("testWebhook1", "http://test1", eventsWebhook1);
        testWebhook2 = new Webhook("testWebhook2", "http://test2", eventsWebhook2);

        staleList = new WebhookSet();
        staleList.add(testWebhook1);
    }

    /**
     * We do not remove Webhooks in our application.
     * This is why we expect 1 change instead of 2.
     */
    @Test
    public void testWebhookChanges() {
        WebhookSet freshlist = new WebhookSet();
        freshlist.addAll(staleList);

        freshlist.add(testWebhook2);
        freshlist.remove(testWebhook1);

        WebhookSet changes = freshlist.getChangesFrom(staleList);

        assertEquals(changes.size(), 1);
        assertFalse(changes.contains(testWebhook1));
        assertTrue(changes.contains(testWebhook2));

    }

}