package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.BitbucketTest;
import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermission;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermissionList;
import com.avisi.bitbucketconfigurator.domain.objects.BitbucketSetup;
import com.avisi.bitbucketconfigurator.domain.objects.ChangeLog;
import com.avisi.bitbucketconfigurator.domain.objects.GroupPermission;
import com.avisi.bitbucketconfigurator.domain.objects.UserPermission;
import com.avisi.bitbucketconfigurator.domain.objects.Webhook;
import com.avisi.bitbucketconfigurator.domain.objects.WebhookSet;
import com.avisi.bitbucketconfigurator.gateway.BitbucketGateway;
import com.cdancy.bitbucket.rest.domain.pullrequest.User;
import com.cdancy.bitbucket.rest.domain.repository.Group;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.avisi.bitbucketconfigurator.domain.objects.PermissionType.ADMIN;
import static com.avisi.bitbucketconfigurator.domain.objects.PermissionType.READ;
import static com.avisi.bitbucketconfigurator.domain.objects.PermissionType.WRITE;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

public class ConfigurationServiceIntegrationTest extends BitbucketTest {
    private final InputStream defaultTestYamlPath = this.getClass().getResourceAsStream("/defaulttestconfig.yaml");
    private final InputStream defaultTestYamlWithoutWebhooksPath = this.getClass().getResourceAsStream("/defaulttestconfigwithoutwebhooks.yaml");
    private final InputStream defaultTestYamlWithoutUsersPath = this.getClass().getResourceAsStream("/defaulttestconfigwithoutusers.yaml");
    private final InputStream defaultTestYamlWithoutGroupsPath = this.getClass().getResourceAsStream("/defaulttestconfigwithoutgroups.yaml");
    private final InputStream emptyYamlPath = this.getClass().getResourceAsStream("/empty.yaml");

    private final String urlToBitbucket = "/rest/api/1.0/projects/PROJECT/repos/rep_1/";

    private BitbucketSetup bitbucketSetup;

    private ConfigurationService configurationService;

    private List<ActorPermission> actorPermissions;
    private List<ActorPermission> onlyDefaultActorPermissions;
    private List<ActorPermission> onlyGroupPermissions;
    private List<ActorPermission> onlyUserPermissions;
    private List<ActorPermission> noPermissions;

    private Set<Webhook> webhooks;
    private Set<Webhook> onlyDefaultWebhooks;
    private Set<Webhook> noWebhooks;

    private String yamlString;
    private String yamlStringWithoutWebhooks;
    private String yamlStringWithoutUsers;
    private String yamlStringWithoutGroups;
    private String emptyYamlString = "";

    @Before
    public void setUp() throws URISyntaxException, IOException {
        yamlString = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("testconfig.yaml").toURI())));

        final String yamlStringWithoutWebhooksPath = "testconfigwithoutwebhooks.yaml";
        yamlStringWithoutWebhooks = new String(Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource(yamlStringWithoutWebhooksPath).toURI())));

        final String yamlStringWithoutUsersPath = "testconfigwithoutusers.yaml";
        yamlStringWithoutUsers = new String(Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource(yamlStringWithoutUsersPath).toURI())));

        final String yamlStringWithoutGroupsPath = "testconfigwithoutgroups.yaml";
        yamlStringWithoutGroups = new String(Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource(yamlStringWithoutGroupsPath).toURI())));



        YamlService yamlService = new YamlService();
        BitbucketGateway bitbucketGateway = new BitbucketGateway();

        bitbucketSetup = new BitbucketSetup(client, "PROJECT", "rep_1");
        configurationService = new ConfigurationService(yamlService, bitbucketGateway);

        stubFor(get(urlEqualTo(String.format("%swebhooks", urlToBitbucket))).willReturn(aResponse().withBodyFile("/emptyPageResponse.json")));
        stubFor(post(urlEqualTo(String.format("%swebhooks", urlToBitbucket))).willReturn(aResponse().withStatus(201)));
        stubFor(put(urlEqualTo(String.format("%spermissions/groups?permission=REPO_WRITE&name=group1", urlToBitbucket))).willReturn(aResponse().withStatus(200)));
        stubFor(put(urlEqualTo(String.format("%spermissions/users?permission=REPO_WRITE&name=user", urlToBitbucket))).willReturn(aResponse().withStatus(200)));
        stubFor(put(urlEqualTo(String.format("%spermissions/users?permission=REPO_ADMIN&name=Paul", urlToBitbucket))).willReturn(aResponse().withStatus(200)));
        stubFor(put(urlEqualTo(String.format("%spermissions/users?permission=REPO_READ&name=user32", urlToBitbucket))).willReturn(aResponse().withStatus(200)));
        stubFor(put(urlEqualTo(String.format("%spermissions/users?permission=REPO_ADMIN&name=henk", urlToBitbucket))).willReturn(aResponse().withStatus(200)));
        stubFor(get(urlEqualTo(String.format("%spermissions/groups", urlToBitbucket))).willReturn(aResponse().withBodyFile("/emptyPageResponse.json")));
        stubFor(get(urlEqualTo(String.format("%spermissions/users", urlToBitbucket))).willReturn(aResponse().withBodyFile("/emptyPageResponse.json")));

        actorPermissions = new ArrayList<>();
        onlyDefaultActorPermissions = new ArrayList<>();
        onlyGroupPermissions = new ArrayList<>();
        onlyUserPermissions = new ArrayList<>();
        noPermissions = new ArrayList<>();

        webhooks = new HashSet<>();
        onlyDefaultWebhooks = new HashSet<>();
        noWebhooks = new HashSet<>();

        User user32;
        User henk;
        User paul;
        User user;
        Group group1;

        ActorPermission user32Permission;
        ActorPermission henkPermission;
        ActorPermission paulPermission;
        ActorPermission userPermission;
        ActorPermission group1Permission;

        user32 = User.create("user32", "", 0, "", true, "", "");
        henk = User.create("henk", "", 0, "", true, "", "");
        paul = User.create("Paul", "", 0, "", true, "", "");
        user = User.create("user", "", 0, "", true, "", "");
        group1 = Group.create("group1");

        user32Permission = new UserPermission(user32, READ);
        henkPermission = new UserPermission(henk, ADMIN);
        paulPermission = new UserPermission(paul, ADMIN);
        userPermission = new UserPermission(user, WRITE);
        group1Permission = new GroupPermission(group1, WRITE);

        actorPermissions.add(user32Permission);
        actorPermissions.add(henkPermission);
        actorPermissions.add(paulPermission);
        actorPermissions.add(userPermission);
        actorPermissions.add(group1Permission);

        onlyDefaultActorPermissions.add(user32Permission);
        onlyDefaultActorPermissions.add(paulPermission);
        onlyDefaultActorPermissions.add(userPermission);
        onlyDefaultActorPermissions.add(group1Permission);

        onlyUserPermissions.add(user32Permission);
        onlyUserPermissions.add(henkPermission);
        onlyUserPermissions.add(paulPermission);
        onlyUserPermissions.add(userPermission);

        onlyGroupPermissions.add(group1Permission);

        List<String> eventsNaamWebhook2 = new ArrayList<>();
        List<String> eventsNaamWebhook33 = new ArrayList<>();
        List<String> eventsNaamWebhook22 = new ArrayList<>();

        eventsNaamWebhook2.add("repo:refs_changed");
        eventsNaamWebhook33.add("repo:refs_changed");
        eventsNaamWebhook22.add("repo:refs_changed");

        Webhook naamWebhook2 = new Webhook("naamWebhook2", "http://example.com:8080/", eventsNaamWebhook2);
        Webhook naamWebhook33 = new Webhook("naamWebhook33", "http://example.com:8080/webhook/push", eventsNaamWebhook33);
        Webhook naamWebhook22 = new Webhook("naamWebhook22", "http://example/webhook/push", eventsNaamWebhook22);

        webhooks.add(naamWebhook2);
        webhooks.add(naamWebhook33);
        webhooks.add(naamWebhook22);

        onlyDefaultWebhooks.add(naamWebhook2);
    }

    @Test
    public void testIfChangeRequestsGetMade() throws BitbucketCommunicationException, ConfigFileException {
        //Arrange

        List<LoggedRequest> groupRequestsBeforeList = findAll(putRequestedFor(urlMatching(String.format("%spermissions/groups?.*", urlToBitbucket))));
        List<LoggedRequest> userRequestsBeforeList = findAll(putRequestedFor(urlMatching(String.format("%spermissions/users?.*", urlToBitbucket))));
        List<LoggedRequest> webhookRequestsBeforeList = findAll(postRequestedFor(urlMatching(String.format("%swebhooks", urlToBitbucket))));

        int groupRequestsBefore = groupRequestsBeforeList.size();
        int userRequestsBefore = userRequestsBeforeList.size();
        int webhookRequestsBefore = webhookRequestsBeforeList.size();

        //Act
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlString, defaultTestYamlPath);

        List<LoggedRequest> groupRequestsAfterList = findAll(putRequestedFor(urlMatching(String.format("%spermissions/groups?.*", urlToBitbucket))));
        List<LoggedRequest> userRequestsAfterList = findAll(putRequestedFor(urlMatching(String.format("%spermissions/users?.*", urlToBitbucket))));
        List<LoggedRequest> webhookRequestsAfterList = findAll(postRequestedFor(urlMatching(String.format("%swebhooks", urlToBitbucket))));

        //Assert
        assertEquals(groupRequestsBefore+1, groupRequestsAfterList.size());
        assertEquals(userRequestsBefore+4, userRequestsAfterList.size());
        assertEquals(webhookRequestsBefore+3, webhookRequestsAfterList.size());
    }

    @Test
    public void testIfConfigFilesAreProperlyMerged() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlString, defaultTestYamlPath);

        WebhookSet newSet = new WebhookSet();
        ActorPermissionList actorPermissionList = new ActorPermissionList();
        changeLog.getWebhookSet().forEach(change -> newSet.add((Webhook) change.getNewObject()));

        changeLog.getActorPermissionList().forEach(change -> actorPermissionList.add((ActorPermission) change.getNewObject()));

        assertEquals(webhooks, newSet);
        assertEquals(actorPermissions, actorPermissionList);
    }
    @Test
    public void testIfFilesAreProperlyMergedWithOnlyTheWebhooksFromDefaults() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlStringWithoutWebhooks, defaultTestYamlPath);

        WebhookSet newSet = new WebhookSet();
        ActorPermissionList actorPermissionList = new ActorPermissionList();
        changeLog.getWebhookSet().forEach(change -> newSet.add((Webhook) change.getNewObject()));

        changeLog.getActorPermissionList().forEach(change -> actorPermissionList.add((ActorPermission) change.getNewObject()));

        assertEquals(onlyDefaultWebhooks, newSet);
        assertEquals(actorPermissions, actorPermissionList);
    }

    @Test
    public void testIfFilesAreProperlyMergedWithoutWebhooks() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlStringWithoutWebhooks, defaultTestYamlWithoutWebhooksPath);

        WebhookSet newSet = new WebhookSet();
        ActorPermissionList actorPermissionList = new ActorPermissionList();
        changeLog.getWebhookSet().forEach(change -> newSet.add((Webhook) change.getNewObject()));

        changeLog.getActorPermissionList().forEach(change -> actorPermissionList.add((ActorPermission) change.getNewObject()));

        assertEquals(noWebhooks, newSet);
        assertEquals(actorPermissions, actorPermissionList);
    }

    @Test
    public void testIfFilesAreProperlyMergedWithOnlyTheUsersFromDefaults() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlStringWithoutUsers, defaultTestYamlPath);

        WebhookSet newSet = new WebhookSet();
        ActorPermissionList actorPermissionList = new ActorPermissionList();
        changeLog.getWebhookSet().forEach(change -> newSet.add((Webhook) change.getNewObject()));

        changeLog.getActorPermissionList().forEach(change -> actorPermissionList.add((ActorPermission) change.getNewObject()));

        assertEquals(webhooks, newSet);
        assertEquals(onlyDefaultActorPermissions, actorPermissionList);
    }

    @Test
    public void testIfFilesAreProperlyMergedWithoutUsers() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlStringWithoutUsers, defaultTestYamlWithoutUsersPath);

        WebhookSet newSet = new WebhookSet();
        ActorPermissionList actorPermissionList = new ActorPermissionList();
        changeLog.getWebhookSet().forEach(change -> newSet.add((Webhook) change.getNewObject()));

        changeLog.getActorPermissionList().forEach(change -> actorPermissionList.add((ActorPermission) change.getNewObject()));

        assertEquals(webhooks, newSet);
        assertEquals(onlyGroupPermissions, actorPermissionList);
    }

    @Test
    public void testIfFilesAreProperlyMergedWithOnlyTheGroupsFromDefaults() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlStringWithoutGroups, defaultTestYamlPath);

        WebhookSet newSet = new WebhookSet();
        ActorPermissionList actorPermissionList = new ActorPermissionList();
        changeLog.getWebhookSet().forEach(change -> newSet.add((Webhook) change.getNewObject()));

        changeLog.getActorPermissionList().forEach(change -> actorPermissionList.add((ActorPermission) change.getNewObject()));

        assertEquals(webhooks, newSet);
        assertEquals(actorPermissions, actorPermissionList);
    }

    @Test
    public void testIfFilesAreProperlyMergedWithoutGroups() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, yamlStringWithoutGroups, defaultTestYamlWithoutGroupsPath);

        WebhookSet newSet = new WebhookSet();
        ActorPermissionList actorPermissionList = new ActorPermissionList();
        changeLog.getWebhookSet().forEach(change -> newSet.add((Webhook) change.getNewObject()));

        changeLog.getActorPermissionList().forEach(change -> actorPermissionList.add((ActorPermission) change.getNewObject()));

        assertEquals(webhooks, newSet);
        assertEquals(onlyUserPermissions, actorPermissionList);
    }

    @Test(expected = ConfigFileException.class)
    public void testIfEmptyFilesAreProperlyMerged() throws BitbucketCommunicationException, ConfigFileException {
        ChangeLog changeLog = configurationService.updateAllConfigurations(bitbucketSetup, emptyYamlString, emptyYamlPath);
    }
}