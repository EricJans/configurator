package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.BitbucketTest;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermissionList;
import com.avisi.bitbucketconfigurator.domain.objects.ChangeLog;
import com.avisi.bitbucketconfigurator.domain.objects.ErrorLog;
import com.avisi.bitbucketconfigurator.domain.objects.Log;
import com.avisi.bitbucketconfigurator.domain.objects.WebhookSet;
import com.avisi.bitbucketconfigurator.gateway.BitbucketGateway;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class ReapplyAllServiceIntegrationTest extends BitbucketTest {

    private ReapplyAllService service;
    private ConfigurationService configurationService;
    private PayloadService payloadService;
    private LoggingService loggingService;
    private YamlService yamlService;

    @Before
    public void setUp() throws Exception {

        //default to a response to 'mock away' actually updating data, because we don't test that here
        stubFor(put(urlMatching(".*"))
                .willReturn(aResponse()));
        stubFor(delete(urlMatching(".*"))
                .willReturn(aResponse()));

        final String projectsUrl = "/rest/api/1.0/projects";
        returnPageFromUrl(projectsUrl, "projectPage.json");
        returnPageFromUrl(String.format("%s/PRJ/repos",projectsUrl), "repositoryPage.json");
        returnPageFromUrl(String.format("%s/PRJ/repos/my-repo/permissions/users",projectsUrl), "user-permissions.json");
        returnPageFromUrl(String.format("%s/PRJ/repos/my-repo/permissions/groups",projectsUrl), "group-permissions.json");

        yamlService = new YamlService();
        payloadService = new PayloadService(clientService, loggingService, configurationService, "config.yaml");
        BitbucketGateway bitbucketGateway = mock(BitbucketGateway.class);
        configurationService = spy(new ConfigurationService(
                yamlService, bitbucketGateway
        ));
        doReturn(new ActorPermissionList()).when(configurationService).getOnlinePermissions(any());
        doReturn(new WebhookSet()).when(bitbucketGateway).retrieveWebhooks(any());

        service = new ReapplyAllService(clientService, configurationService, payloadService);
    }

    @Test
    public void testUpdateAllConfigurationsIfErrorLogGetsCreatedAfterGivingInvalidYaml() throws JsonProcessingException {
        // Arrange
        final int expectedRepositoryLogs = 1;

        returnPageFromUrl("/projects/PRJ/repos/my-repo/raw/config.yaml", "invalid.yaml");

        // Act
        List<Log> logs = service.updateAllConfigurations();

        //Assert
        assertEquals(expectedRepositoryLogs, logs.size());

        ErrorLog errorLog = logs.get(0).getErrorLog();
        assertNotNull(errorLog);
    }

    @Test
    public void testUpdateAllConfigurationsIfCorrectChangeLogGetsCreatedAfterPassingValidYaml() throws JsonProcessingException {
        //Arrange
        final int expectedRepositoryLogs = 1;
        final int expectedActorPermisionListSize = 7;
        final int expectedWebhooksListSize = 2;

        //Act
        returnPageFromUrl("/projects/PRJ/repos/my-repo/raw/config.yaml", "reapply-config.yaml");
        List<Log> logs = service.updateAllConfigurations();

        //Assert
        assertEquals(expectedRepositoryLogs, logs.size());

        ChangeLog changeLog = logs.get(0).getChangeLog();
        assertNotNull(changeLog);

        assertEquals(expectedActorPermisionListSize, changeLog.getActorPermissionList().size());
        assertEquals(expectedWebhooksListSize, changeLog.getWebhookSet().size());
    }

}