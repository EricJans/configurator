package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.BitbucketTest;
import com.avisi.bitbucketconfigurator.dao.MongoLogDao;
import com.avisi.bitbucketconfigurator.gateway.BitbucketGateway;
import com.avisi.bitbucketconfigurator.rest.RepoConfigValidEndpoint;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.yaml.snakeyaml.Yaml;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class RepoConfigValidEndpointTest extends BitbucketTest {

    private BitbucketClientService service;
    private BitbucketGateway bitbucketGateway;

    private RepoConfigValidEndpoint repoConfigValidEndpoint;
    private String projectsUrl;

    @Before
    public void setUp() throws Exception {
        projectsUrl = "/projects";

        service = new BitbucketClientService("http://localhost:2345", "username", "password");

        stubFor(get(urlEqualTo("/rest/api/1.0/admin/users")).willReturn(aResponse().withBodyFile("/userPagePayloadResponse.json")));
        stubFor(get(urlEqualTo("/rest/api/1.0/admin/groups")).willReturn(aResponse().withBodyFile("/groupPagePayloadResponse.json")));

        LoggingService loggingService = mock(LoggingService.class);
        ConfigurationService configurationService = mock(ConfigurationService.class);
        PayloadService payloadService = new PayloadService(service, loggingService, configurationService, "config.yaml");
        bitbucketGateway = new BitbucketGateway();
        YamlService yamlService = new YamlService(bitbucketGateway);
        repoConfigValidEndpoint = new RepoConfigValidEndpoint(payloadService, yamlService, service);

    }

    @Test
    public void testValidConfigFile() throws JsonProcessingException {
        returnPageFromUrl(String.format("%s/PRJ/repos/my-repo/raw/config.yaml", projectsUrl), "repo-config-valid.yaml");

        ResponseEntity<String> responseEntity = repoConfigValidEndpoint.validateEndpoint("PRJ", "my-repo");

        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testInvalidConfigFile() throws JsonProcessingException {
        returnPageFromUrl(String.format("%s/PRJ/repos/my-repo/raw/config.yaml", projectsUrl), "invalid.yaml");

        ResponseEntity<String> responseEntity = repoConfigValidEndpoint.validateEndpoint("PRJ", "my-repo");

        assertEquals(401, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testNoConfigFile() throws JsonProcessingException {
        returnPageFromUrl(String.format("%s/PRJ/repos/my-repo/raw/config.yaml", projectsUrl), "reapply-config-wrong-name.yaml");

        ResponseEntity<String> responseEntity = repoConfigValidEndpoint.validateEndpoint("PRJ", "my-repo");

        assertEquals(401, responseEntity.getStatusCodeValue());
        assertTrue(responseEntity.getBody().toLowerCase().contains("file content"));
    }

    @Test
    public void testInvalidConfigFileBecauseUserDoesNotExist() throws JsonProcessingException {
        returnPageFromUrl(String.format("%s/PRJ/repos/my-repo/raw/config.yaml", projectsUrl), "reapply-config.yaml");

        ResponseEntity<String> responseEntity = repoConfigValidEndpoint.validateEndpoint("PRJ", "my-repo");

        assertEquals(401, responseEntity.getStatusCodeValue());
        assertTrue(responseEntity.getBody().toLowerCase().contains("user"));
    }
}
