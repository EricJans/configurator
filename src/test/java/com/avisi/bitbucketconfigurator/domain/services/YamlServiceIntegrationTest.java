package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.BitbucketTest;
import com.avisi.bitbucketconfigurator.domain.exception.BitbucketCommunicationException;
import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.Group;
import com.avisi.bitbucketconfigurator.gateway.BitbucketGateway;
import com.cdancy.bitbucket.rest.domain.pullrequest.User;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class YamlServiceIntegrationTest extends BitbucketTest {

    private BitbucketClientService service;
    private YamlService yamlService;
    private BitbucketGateway bitbucketGateway;

    @Before
    public void setUp() {
        bitbucketGateway = new BitbucketGateway();
        yamlService = new YamlService(bitbucketGateway);
        service = new BitbucketClientService("http://localhost:2345", "username","password");
        stubFor(get(urlEqualTo("/rest/api/1.0/admin/groups")).willReturn(aResponse().withBodyFile("/groupPagePayloadResponse.json")));
        stubFor(get(urlEqualTo("/rest/api/1.0/admin/users")).willReturn(aResponse().withBodyFile("/userPagePayloadResponse.json")));
    }

    @Test
    public void checkIfUserDataGetsRetrievedFromTheBitbucketServer() {
        //Act
        List<User> bitbucketUserList = service.getBitbucketClient().api().adminApi().listUsers(null, null, null).values();
        //Assert
        assertEquals(1, bitbucketUserList.size());
        assertEquals("jcitizen", bitbucketUserList.get(0).name());
    }

    @Test
    public void checkIfGroupsGetsRetrievedFromTheBitbucketServer() throws BitbucketCommunicationException {
        //Act
        List<Group> bitbucketGroupList = bitbucketGateway.retrieveGroups(service);

        //Assert
        assertEquals(1, bitbucketGroupList.size());
        assertEquals("group1", bitbucketGroupList.get(0).getName());
    }

    @Test
    public void checkIfUsersInConfigurationDoNotMatchTheUsersOnTheBitbucketServer() throws IOException, BitbucketCommunicationException {
        //Arrange
        byte[] bytes = null;
        String yamlContent = null;
        final String errorMsg = "User(s) not found: [Paul, user32, user]";

        try {
            //Act
            bytes = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource("checkusermatch.yaml").toURI()));
            yamlContent =  new String(bytes);
            yamlService.validateYaml(yamlContent, service);
            fail("Should not get here, test failed if you read this.");
        }
        catch (URISyntaxException e) {
             e.printStackTrace();
        }
        catch(ConfigFileException e){
            //Assert
             assertEquals(errorMsg, e.getMessage());
        }
    }

    @Test
    public void checkIfGroupsInConfigurationDoNotMatchTheGroupsOnTheBitbucketServer() throws IOException, BitbucketCommunicationException, ConfigFileException {
        //Arrange
        byte[] bytes = null;
        String yamlContent = null;
        final String errorMsg = "User(s) not found: [admin, user] AND Group(s) not found: [group4]";

        try{
            //Act
            bytes = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource("checkgroupmatch.yaml").toURI()));
            yamlContent = new String(bytes);
            yamlService.validateYaml(yamlContent, service);
            fail("Should not get here, test failed if you read this.");
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
        }
        catch(ConfigFileException e){
            //Assert
            assertEquals(errorMsg, e.getMessage());
        }
    }
}