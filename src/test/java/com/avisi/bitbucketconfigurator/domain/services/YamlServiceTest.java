package com.avisi.bitbucketconfigurator.domain.services;

import com.avisi.bitbucketconfigurator.domain.exception.ConfigFileException;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermission;
import com.avisi.bitbucketconfigurator.domain.objects.ActorPermissionList;
import com.avisi.bitbucketconfigurator.domain.objects.PermissionType;
import com.avisi.bitbucketconfigurator.domain.objects.configfile.ConfigFile;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class YamlServiceTest {

    private YamlService yamlService;

    @Before
    public void setup(){
        yamlService = new YamlService();
    }

    @Test
    public void testListTranslation() throws ConfigFileException {
        final String correctYaml = "permissions: \n" +
                "  groups: \n" +
                "    group1: WRITE\n" +
                "  users: \n" +
                "    Paul: WRITE\n" +
                "    admin: ADMIN\n" +
                "    user: WRITE\n";

        ConfigFile configFile = yamlService.deserializeYaml(correctYaml);
        ActorPermissionList list = configFile.createPermissionList();

        assertEquals(list.size(), 4);

        boolean sysadminFound = false;
        boolean paulFound = false;
        for (ActorPermission userActorPermission : list) {
            if (userActorPermission.getName().toLowerCase().equals("paul")) {
                paulFound = true;
            }

            if (userActorPermission.getType().equals(PermissionType.ADMIN)) {
                sysadminFound = true;
            }
        }
        assertTrue(sysadminFound && paulFound);
    }

    @Test(expected = ConfigFileException.class)
    public void testThrowsExceptionOnInvalidYamlString() throws Exception {
        yamlService.deserializeYaml("[::}");
    }

    @Test(expected = ConfigFileException.class)
    public void testValidYaml() throws ConfigFileException {
        final String badYaml = "[::]";
        yamlService.deserializeYaml(badYaml);
    }

    @Test(expected = ConfigFileException.class)
    public void testInvalidYamlOnGetPermissionChangesFromString() throws ConfigFileException {
        final String badYaml = "[::]";
        yamlService.deserializeYaml(badYaml);
    }
}