package com.avisi.bitbucketconfigurator.rest;

import com.avisi.bitbucketconfigurator.domain.services.PayloadService;
import com.avisi.bitbucketconfigurator.rest.dto.PushPayloadDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class WebhookEndpointTest {

    private WebhookEndpoint webhookEndpoint;
    private PayloadService payloadService;

    @Before
    public void setup() {
        // Mock service away
        payloadService = mock(PayloadService.class);
        doNothing().when(payloadService).handle(any());

        webhookEndpoint = new WebhookEndpoint(payloadService);
    }

    @Test
    public void testWebhookPushIfConfigurationUpdateServiceCalledAfterPassingCorrectPayload() throws IOException {
        // Arrange
        final String correctPayloadJson = "{\"eventKey\":\"repo:refs_changed\",\"date\":\"2017-12-04T15:54:28+0100\",\"actor\":{\"name\":\"admin\",\"emailAddress\":\"admin@example.com\",\"id\":1,\"displayName\":\"Administrator\",\"active\":true,\"slug\":\"admin\",\"type\":\"NORMAL\"},\"repository\":{\"slug\":\"simple\",\"id\":23,\"name\":\"simple\",\"scmId\":\"git\",\"state\":\"AVAILABLE\",\"statusMessage\":\"Available\",\"forkable\":true,\"project\":{\"key\":\"TEST\",\"id\":42,\"name\":\"Test\",\"public\":false,\"type\":\"NORMAL\"},\"public\":false},\"changes\":[{\"ref\":{\"id\":\"refs/heads/master\",\"displayId\":\"master\",\"type\":\"BRANCH\"},\"refId\":\"refs/heads/master\",\"fromHash\":\"ec39b08af1ee5f6f0106a4fcfe608b8d699fb4bf\",\"toHash\":\"45a6e3c36bf4825e0aa6eafffd83a73e021541f6\",\"type\":\"UPDATE\"}]}\n";

        final ArgumentCaptor<PushPayloadDTO> argument = ArgumentCaptor.forClass(PushPayloadDTO.class);

        // Act
        webhookEndpoint.onWebhookPush(correctPayloadJson);

        // Assert
        verify(payloadService, times(1)).handle(argument.capture());
    }

    @Test
    public void testWebhookPushIfConfigurationUpdateServiceNotCalledAfterPassingIncorrectPayload() throws IOException {
        // Arrange
        final String incorrectPayloadJson = "\"eventKey\":\"repo:Name\":\"Administratord\":23,\"name\":\"simple\",\"scmId\":\"git\",\"state\":\"AVAILABLE\",\"statusMessage\":\"Available\",\"forkable\":true,\"project\":{\"key\":\"TEST\",\"id\":42,\"name\":\"Test\",\"public\":false,\"type\":\"NORMAL\"},\"public\":false},\"changes\":[{\"ref\":{\"id\":\"refs/heads/master\",\"displayId\":\"master\",\"type\":\"BRANCH\"},\"refId\":\"refs/heads/master\",\"fromHash\":\"ec39b08af1ee5f6f0106a4fcfe608b8d699fb4bf\",\"toHash\":\"45a6e3c36bf4825e0aa6eafffd83a73e021541f6\",\"type\":\"UPDATE\"}]}\n";

        final ArgumentCaptor<PushPayloadDTO> argument = ArgumentCaptor.forClass(PushPayloadDTO.class);

        // Act
        webhookEndpoint.onWebhookPush(incorrectPayloadJson);

        // Assert
        verify(payloadService, times(0)).handle(argument.capture());
    }

}