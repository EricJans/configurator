package com.avisi.bitbucketconfigurator.rest;

import com.avisi.bitbucketconfigurator.domain.services.BitbucketClientService;
import com.avisi.bitbucketconfigurator.domain.services.YamlService;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class YamlValidatorEndpointTest {

    private YamlValidatorEndpoint yamlValidatorEndpoint;
    private BitbucketClientService bitbucketClientService;
    private YamlService yamlService;

    private final int expectedBadRequestCode = 400;


    @Before
    public void setup(){
        yamlService = new YamlService();
        bitbucketClientService = new BitbucketClientService(null, null, null);
        yamlValidatorEndpoint = new YamlValidatorEndpoint(yamlService, bitbucketClientService);
    }

    @Test
    public void testValidateYamlIfYamlIsInvalid() {
        //Arrange
        final String badYaml = "[::]";
        //Act
        final int statusCodeInvalidYamlSyntax = yamlValidatorEndpoint.validateYaml(badYaml).getStatusCodeValue();
        //Assert
        assertEquals(expectedBadRequestCode, statusCodeInvalidYamlSyntax);
    }

    @Test
    public void testValidateYamlIfYamlCanNotBeMappedProperly() {
        //Arrange
        final String nonMappableYaml = "permissions: \n" +
                "  groups: \n" +
                "    group1\n" +
                "  users: \n" +
                "    Paul: WRITE\n" +
                "    admin: \n" +
                "    user: WRITE\n";
        //Act
        final int statusCodeNotProperlyMappedYaml = yamlValidatorEndpoint.validateYaml(nonMappableYaml).getStatusCodeValue();
        //Assert
        assertEquals(expectedBadRequestCode, statusCodeNotProperlyMappedYaml);
    }

}